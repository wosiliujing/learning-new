package com.wosiliujing.learning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearningCommonSentinelApplication {

    public static void main(String[] args) {
        SpringApplication.run(LearningCommonSentinelApplication.class, args);
    }

}
