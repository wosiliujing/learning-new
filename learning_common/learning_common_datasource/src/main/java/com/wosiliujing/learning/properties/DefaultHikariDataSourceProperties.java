package com.wosiliujing.learning.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author 刘靖
 * @date 2019-10-30 16:07
 */
@Data
@Component
@ConfigurationProperties(prefix = "spring.datasource")
public class DefaultHikariDataSourceProperties{
    private String username;
    private String password;
    private String url;
    private String driverClassName;

}

