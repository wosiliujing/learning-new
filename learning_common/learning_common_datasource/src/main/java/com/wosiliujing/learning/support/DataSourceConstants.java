package com.wosiliujing.learning.support;

/**
 * @author 刘靖
 * @date 2019-10-30 16:51
 */
public interface DataSourceConstants {

    /**
     * 默认数据源
     */
    String DS_DEFAULT = "default";

    /**
     * 备用数据源1
     */
    String DS_SLAVE_ONE = "slave1";

    /**
     * 备用数据源2
     */
    String DS_SLAVE_TWO = "slave2";

}
