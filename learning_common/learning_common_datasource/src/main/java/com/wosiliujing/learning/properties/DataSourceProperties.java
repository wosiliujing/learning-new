package com.wosiliujing.learning.properties;

import lombok.Data;

/**
 * @author 刘靖
 * @date 2019-10-31 11:29
 */
@Data
public class DataSourceProperties {
    private String type;
    private String username;
    private String password;
    private String url;
    private String driverClassName;
}
