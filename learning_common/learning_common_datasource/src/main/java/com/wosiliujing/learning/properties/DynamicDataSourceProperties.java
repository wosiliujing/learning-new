package com.wosiliujing.learning.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author 刘靖
 * @date 2019-10-31 11:32
 */
@Data
@Component
@RefreshScope
@ConfigurationProperties(prefix = "dynamic")
public class DynamicDataSourceProperties {
    private Map<String,DataSourceProperties> dataSource = new LinkedHashMap<>();
}
