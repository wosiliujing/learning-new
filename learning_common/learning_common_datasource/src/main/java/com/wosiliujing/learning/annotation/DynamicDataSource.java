package com.wosiliujing.learning.annotation;

import java.lang.annotation.*;

/**
 * @author 刘靖
 * @date 2019-11-01 14:27
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface DynamicDataSource {
    String value() default "";
}
