package com.wosiliujing.learning.support;

import lombok.experimental.UtilityClass;

/**
 * @author 刘靖
 * @date 2019-10-30 16:17
 */
@UtilityClass
public class DynamicDataSourceContextHolder {

    private final ThreadLocal<String> CONTEXT_HOLDER = new ThreadLocal<>();

    /**
     * 设置当前数据源标识
     * @param dataSourceType 数据源标识
     */
    public void setDataSourceType(String dataSourceType) {
        CONTEXT_HOLDER.set(dataSourceType);
    }

    /**
     * 获取当前数据源标识
     * @return
     */
    public String getDataSourceType() {
        return CONTEXT_HOLDER.get();
    }

    /**
     * 清楚数据设置(恢复默认数据源)
     */
    public void clearDataSourceType() {
        CONTEXT_HOLDER.remove();
    }
}

