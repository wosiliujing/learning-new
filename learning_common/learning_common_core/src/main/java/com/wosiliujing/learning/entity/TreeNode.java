package com.wosiliujing.learning.entity;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: liujing
 * @date: 2019/5/7 16:21
 * @description:
 */
@Data
public class TreeNode {

    protected String id;

    protected String parentId;

    protected List<TreeNode> children = new ArrayList<>();

    public void add(TreeNode treeNode){
        children.add(treeNode);
    }
}
