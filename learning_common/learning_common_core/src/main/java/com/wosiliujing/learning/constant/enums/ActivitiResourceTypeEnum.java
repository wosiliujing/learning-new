package com.wosiliujing.learning.constant.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author: liujing
 * @date: 2019/6/4 18:56
 * @description:
 */
@Getter
@AllArgsConstructor
public enum ActivitiResourceTypeEnum {
    /**
     * 图片资源
     */
    IMAGE("image", "图片资源"),

    /**
     * xml资源
     */
    XML("xml", "xml资源");

    /**
     * 类型
     */
    private final String type;
    /**
     * 描述
     */
    private final String description;
}
