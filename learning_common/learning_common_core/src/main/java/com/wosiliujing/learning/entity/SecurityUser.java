package com.wosiliujing.learning.entity;

import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;
import java.util.Map;

/**
 * @author 刘靖
 * @date 2020-06-04 16:48
 */
public class SecurityUser extends User {

    /**
     * 附加参数
     */
    @Getter
    private Map<String,Object> enhanceMap;


    public SecurityUser(Map<String,Object> enhanceMap,String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled,accountNonExpired,credentialsNonExpired,accountNonLocked,authorities);
        this.enhanceMap = enhanceMap;
    }
}
