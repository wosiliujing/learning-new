package com.wosiliujing.learning.util;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author 刘靖
 * @date 2020-06-11 15:06
 */
@Data
@ApiModel(value = "分页结果信息")
public class PageResult<T> {

    /**
     * 分页数据
     */
    @ApiModelProperty(value = "分页数据")
    private List<T> records;

    /**
     * 总数
     */
    @ApiModelProperty(value = "总数")
    private long total;

    /**
     * 每页数量
     */
    @ApiModelProperty(value = "每页数量")
    private long size;

    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页")
    private long current;

    /**
     * 总页数
     */
    @ApiModelProperty(value = "总页数")
    private long pages;
}
