package com.wosiliujing.learning.constant;

/**
 * 登陆类型常量
 * @author 刘靖
 * @date 2019-09-24 9:07
 */
public interface LoginTypeConstants {

    /**
     * 登陆类型
     */
    String TYPE = "type";

    /**
     * 登陆标识
     */
    String  CODE = "code";

    /**
     * 验证码
     */
    String RAM_STR = "ramStr";

    /**
     * 账号密码登陆
     */
    String PWD = "PWD";

    /**
     * 验证码登陆
     */
    String SMS = "SMS";


}
