package com.wosiliujing.learning.constant.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author: liujing
 * @date: 2019/6/4 18:29
 * @description:
 */
@AllArgsConstructor
@Getter
public enum ProcessStatusEnum {

    /**
     * 激活
     */
    ACTIVE("active", "激活"),

    /**
     * 挂起
     */
    SUSPEND("suspend", "挂起");

    /**
     * 类型
     */
    private final String status;
    /**
     * 描述
     */
    private final String description;
}
