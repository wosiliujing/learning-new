package com.wosiliujing.learning.constant;

/**
 * @author: liujing
 * @date: 2019/5/6 16:27
 * @description:
 */
public interface SecurityConstants {

    /**
     * 资源服务器bean名称
     */
    String RESOURCE_SERVER_CONFIGURER = "resourceServerConfigurerAdapter";

    /**
     * 加密特征码
     */
    String PASSWORD_ENCODER_TYPE = "{bcrypt}";

    /**
     * token前缀
     */
    String TOKEN_PREFIX = "learning_token:";

    /**
     * 服务鉴权标志
     */
    String FROM = "from";

    /**
     * 是否内部调用
     */
    String FROM_IN = "Y";

    /**
     * token access 前缀
     */
    String TOKEN_PERFIX_ACCESS= TOKEN_PREFIX + "access:";

    String CLIENT_FIELDS_FOR_UPDATE = "resource_ids, scope, "
            + "authorized_grant_types, web_server_redirect_uri, authorities, access_token_validity, "
            + "refresh_token_validity, additional_information, autoapprove";

    String CLIENT_FIELDS = "CONCAT('{noop}',client_secret) as client_secret, " + CLIENT_FIELDS_FOR_UPDATE;

    String BASE_FIND_STATEMENT = "select client_id, " + CLIENT_FIELDS
            + " from sys_oauth_client_details";

    /**
     * 通过id查询客户端
     */
    String DEFAULT_SELECT_STATEMENT = BASE_FIND_STATEMENT + " where client_id = ?";

    /**
     * 默认查询语句
     */
    String DEFAULT_FIND_STATEMENT = BASE_FIND_STATEMENT + " order by client_id";

    /**
     * 客户端模式
     */
    String CLIENT_CREDENTIALS = "client_credentials";

    /**
     * 用户userId属性
     */
    String USER_ID = "userId";

    /**
     * 用户手机
     */
    String PHONE = "phone";

    /**
     * 用户部门id
     */
    String DEPT_ID = "deptId";

    /**
     * sessionId
     */
    String SESSION_ID = "session_id";

    /**
     * redis sessionId key
     */
    String SESSION_REDIS_KEY = "session_live_key:";

    /**
     * 手机登录url
     */
    String MOBILE_TOKEN_URL = "/mobile/token/*";

    /**
     * 验证码位数
     */
    Integer CODE_SIZE = 4;

    /**
     * 附加参数
     */
    String ENHANCE_MAP= "enhanceMap";
}
