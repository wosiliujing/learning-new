package com.wosiliujing.learning.handler;

import com.wosiliujing.learning.util.RestResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.SpringSecurityMessageSource;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolationException;
import java.util.List;


/***
 * 全局异常处理
 * @author 刘靖
 * @date 2019/5/3 21:00
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    /**
     * 全局默认异常
     * @author 刘靖
     * @date 2019/9/18 9:23
     * @param e 系统异常
     * @return  异常返回处理
     */
    @ExceptionHandler({Exception.class})
    public RestResult exception(Exception e) {
        log.error("全局异常信息 ex={}", e.getMessage(), e);
        return  RestResult.error(HttpStatus.INTERNAL_SERVER_ERROR.value(),e.getMessage());
    }


    /**
     * 授权拒绝异常处理
     * @author 刘靖
     * @date 2019/9/18 9:26
     * @param e AccessDeniedException
     * @return 异常处理
     */
    @ExceptionHandler({AccessDeniedException.class})
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public RestResult accessDeniedException(AccessDeniedException e) {
        String msg = SpringSecurityMessageSource.getAccessor()
                .getMessage("AbstractAccessDecisionManager.accessDenied"
                        , e.getMessage());
        log.error("拒绝授权异常信息 ex={}", msg, e);
        return  RestResult.error(HttpStatus.FORBIDDEN.value(),e.getMessage());
    }

    /***
     * 参数校验异常
     * @author 刘靖
     * @date 2019/9/18 10:09
     * @param e  MethodArgumentNotValidException
     * @return  异常处理
     */
    @ExceptionHandler({MethodArgumentNotValidException.class,BindException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public RestResult validException(MethodArgumentNotValidException e){
        List<FieldError> errors = e.getBindingResult().getFieldErrors();
        StringBuilder errorMsg = new StringBuilder();
        for(FieldError fieldError: errors){
            errorMsg.append(fieldError.getDefaultMessage()).append(";");
        }
        log.error("参数异常 ex={}",errorMsg);
        return RestResult.error(HttpStatus.BAD_REQUEST.value(),errorMsg.toString());
    }

    /***
     * 参数校验异常
     * @author 刘靖
     * @date 2019/10/12 15:22
     * @param e 异常
     * @return  异常结果封装
     */
    @ExceptionHandler({ ConstraintViolationException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public RestResult validException(ConstraintViolationException e){
        log.error("参数异常 ex={}",e.getMessage());
        return RestResult.error(HttpStatus.BAD_REQUEST.value(),e.getMessage());
    }


    /***
     * 请求方法不支持封装
     * @author 刘靖
     * @date 2020/3/23 14:53
     * @param e 异常
     * @return  封装信息
     */
    @ExceptionHandler({ HttpRequestMethodNotSupportedException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public RestResult validHttpRequestMethodNotSupportedExceptionException(HttpRequestMethodNotSupportedException e){
        log.error("方法不支持 ex={}",e.getMessage());
        return RestResult.error(HttpStatus.BAD_REQUEST.value(),"请求方式错误");
    }
}
