package com.wosiliujing.learning.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

/**
 * @author 刘靖
 * @date 2019-12-11 11:03
 */
@Data
@EqualsAndHashCode
public class UserInfoDTO implements Serializable {


    /**
     * 用户名
     */
    private String userName;

    /**
     * 密码
     */
    private String password;

    /**
     * 附加参数
     */
    private Map<String,Object> enhanceMap;

    /**
     * 权限
     */
    private Set<String> permission;
}
