package com.wosiliujing.learning.constant;

/**
 * @author: liujing
 * @date: 2019/5/4 17:25
 * @description:
 */
public interface MicroServiceNameConstants {

     String LEARNING_AUTH = "learning-auth";

     String LEARNING_ADMIN = "learning-admin";

}
