package com.wosiliujing.learning.constant;

/**
 * 常用常量类
 * @author 刘靖
 * @date 2020/6/4 16:32
 */
public interface CommonConstants {

    /**
     * 失败标记
     */
    int FAIL = 1;

    /**
     * 成功标记
     */
    int SUCCESS = 0;

    /**
     * 正常状态
     */
    String NORMAL_STATUS = "0";

    /**
     * 删除状态
     */
    String DELETE_STATUS = "1";

    /**
     * 未锁住
     */
    String  NORMAL_LOCK_STATUS = "0";

    /**
     * 已锁住
     */
    String  IN_NORMAL_LOCK_STATUS = "1";
    /**
     * 保存成功信息
     */
    String SAVE_SUCCESS = "保存成功";

    /**
     * 保存失败信息
     */
    String SAVE_FAIL = "保存失败";

    /**
     * 更新成功信息
     */
    String UPDATE_SUCCESS = "更新成功";

    /**
     * 更新失败信息
     */
    String UPDATE_FAIL = "更新失败";

    /**
     * 删除成功信息
     */
    String DELETE_SUCCESS = "删除成功";

    /**
     * 更新失败信息
     */
    String DELETE_FAIL = "删除失败";

    /**
     * 操作成功
     */
    String COMMON_SUCCESS = "操作成功";

    /**
     * 操作失败
     */
    String COMMON_FAIL = "操作失败";

    /**
     * 树的根节点
     */
    String TREE_ROOT = "root";

    /**
     * 页码参数
     */
    String CURRENT ="current";

    /**
     *每页条数参数
     */
    String SIZE = "size";

    /**
     * UTF-8编码
     */
    String UTF8 = "UTF-8";

    /**
     * JSON
     */
    String CONTENT_TYPE = "application/json; charset=utf-8";

    /**
     * 初始化密码
     */
    String INIT_PASSWORD = "123456";

    /**
     * 短信验证码前缀
     */
    String SMS_CODE_KEY_ = "SMS_CODE_KEY_";

    /**
     * 验证码失效时间
     */
    Integer CODE_TIME = 60;
}
