package com.wosiliujing.learning.util;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.wosiliujing.learning.constant.CommonConstants;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 响应信息主体
 * @author 刘靖
 * @date 2020/6/4 16:21
 */
@ToString
@Accessors(chain = true)
@JsonInclude(value=JsonInclude.Include.NON_NULL)
public class RestResult<T>  implements Serializable {
    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    @ApiModelProperty(value = "返回码")
    private int code = CommonConstants.SUCCESS;

    @Getter
    @Setter
    @ApiModelProperty(value = "返回信息")
    private String msg = "success";


    @Getter
    @Setter
    @ApiModelProperty(value = "返回数据")
    private T data;

    public RestResult() {
        super();
    }

    public RestResult(T data) {
        super();
        this.data = data;
    }

    public RestResult(int code, String msg) {
        super();
        this.msg = msg;
        this.code = code;
    }

    public RestResult(int code, String msg, T data) {
        super();
        this.msg = msg;
        this.data = data;
        this.code = code;
    }

    public RestResult(T data, String msg) {
        super();
        this.data = data;
        this.msg = msg;
    }
    public RestResult(String msg, T data) {
        super();
        this.msg = msg;
        this.data = data;
    }


    public static <T> RestResult<T> ok(){
        return restResult(CommonConstants.SUCCESS, "success", null );
    }

    public static RestResult ok(String msg){
        return restResult(CommonConstants.SUCCESS, msg ,null);
    }

    public static <T> RestResult<T> data(String msg,T data){
        return restResult(CommonConstants.SUCCESS,msg,data);
    }

    public static <T> RestResult<T> data(int code,String msg,T data){
        return restResult( code, msg, data);
    }

    public static <T> RestResult<T> data(T data){
        return restResult(CommonConstants.SUCCESS, "success", data);
    }

    public static <T> RestResult<T> error(){
        return restResult(CommonConstants.FAIL, "系统错误，请联系管理员",null);
    }

    public static <T> RestResult<T> error(String msg, T data){
        return restResult(CommonConstants.FAIL, msg, data);
    }

    public static <T> RestResult<T> error(String msg){
        return restResult(CommonConstants.FAIL,msg, null);
    }

    public static <T> RestResult<T> error(int code, String msg){
        return restResult(code, msg, null);
    }

    private static <T> RestResult<T> restResult(int code, String msg, T data) {
        RestResult<T> apiResult = new RestResult<>();
        apiResult.setCode(code);
        apiResult.setData(data);
        apiResult.setMsg(msg);
        return apiResult;
    }

    public static <T> RestResult<PageResult<T>> page(IPage<T> page){
        PageResult<T> pageResult = new PageResult<>();
        pageResult.setCurrent(page.getCurrent());
        pageResult.setPages(page.getPages());
        pageResult.setRecords(page.getRecords());
        pageResult.setSize(page.getSize());
        pageResult.setTotal(page.getTotal());
        return restResult(CommonConstants.SUCCESS, "success", pageResult);
    }

    public static <T,K> RestResult<PageResult<K>> page(IPage<T> page, K targetEntity){
        List<K> current = page.getRecords().stream().map(entity -> {
            K target = null;
            try {
                 target = (K) targetEntity.getClass().newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            BeanUtils.copyProperties(entity, target);
            return target;
        }).collect(Collectors.toList());
        PageResult<K> pageResult = new PageResult<>();
        pageResult.setCurrent(page.getCurrent());
        pageResult.setPages(page.getPages());
        pageResult.setRecords(current);
        pageResult.setSize(page.getSize());
        pageResult.setTotal(page.getTotal());
        return restResult(CommonConstants.SUCCESS, "success", pageResult);
    }
}
