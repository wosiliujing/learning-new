package com.wosiliujing.learning.constant.enums;

import lombok.Getter;

/**
 * @author: liujing
 * @date: 2019/5/8 19:54
 * @description:
 */
public enum ResourcesTypeEnum {
    /**
     * 菜单左
     */
    MENU_LEFT("0"),

    /**
     * 菜单右
     */
    MENU_TOP("1"),

    /**
     * 按钮
     */
    BUTTON("2"),

    /**
     * 功能
     */
    FUNCTION("3");

    @Getter
    private String value;

     ResourcesTypeEnum(String value){
        this.value = value;
    }

}
