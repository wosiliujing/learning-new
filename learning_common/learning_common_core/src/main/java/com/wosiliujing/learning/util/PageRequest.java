package com.wosiliujing.learning.util;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;

/**
 * @author 刘靖
 * @date 2020-06-11 14:26
 */
@Data
@ApiModel(value = "分页参数信息")
public class PageRequest {

    /**
     * 页数
     */
    @Min(value = 1,message = "当前页不能小于1")
    @ApiModelProperty(value = "页数(默认1)")
    private long size = 1;

    /**
     * 当前页
     */
    @Min(value = 1, message = "每页数量不能小于1")
    @ApiModelProperty(value = "当前页(默认10)")
    private long current = 10;

    /**
     * 升序字段
     */
    @ApiModelProperty(value = "升序字段")
    private String[] ascs;

    /**
     * 降序字段
     */
    @ApiModelProperty(value = "降序字段")
    private String[] descs;

    /**
     * 是否优化sql
     */
    @JsonIgnore
    @ApiModelProperty(hidden=true)
    private boolean optimizeCountSql = true;

    /**
     * 是否查询总记录数
     */
    @JsonIgnore
    @ApiModelProperty(value = "是否查询总记录数、默认true")
    private boolean isSearchCount = true;

    /**
     * 转换成plus的Page
     * @return plus的Page
     */
    public <T> Page<T> toSqlRequest() {
        Page<T> page = new Page<>(current, size);
        page.setDesc(descs);
        page.setAsc(ascs);
        page.setOptimizeCountSql(optimizeCountSql);
        page.setSearchCount(isSearchCount);
        return page;
    }
}
