package com.wosiliujing.learning.annotation;

import java.lang.annotation.*;

/***
 * 不鉴权接口注解
 * @author 刘靖
 * @date 2019/12/10 15:17
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Ignore {
}
