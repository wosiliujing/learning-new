package com.wosiliujing.learning.annotation;

import com.wosiliujing.learning.config.LearningResourcesServerAutoConfiguration;
import com.wosiliujing.learning.config.LearningSecurityBeanDefinitionRegistrar;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

import java.lang.annotation.*;

/**
 * @author 刘靖
 * @date 2019/5/1 22:23
 */
@Documented
@Inherited
@EnableResourceServer
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Import({LearningResourcesServerAutoConfiguration.class, LearningSecurityBeanDefinitionRegistrar.class})
public @interface EnableLearningResourceServer {

}
