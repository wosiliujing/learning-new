package com.wosiliujing.learning.annotation;

import java.lang.annotation.*;

/***
 * @author 刘靖
 * @date 2019/5/21 15:04
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Inner {

    /**
     * 是否AOP统一处理
     *
     * @return false, true
     */
    boolean value() default true;
}
