package com.wosiliujing.learning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearningCommonJobApplication {

    public static void main(String[] args) {
        SpringApplication.run(LearningCommonJobApplication.class, args);
    }

}
