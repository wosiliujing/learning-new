package com.wosiliujing.learning.gateway.config;

import org.springframework.cloud.gateway.config.GatewayProperties;
import org.springframework.cloud.gateway.config.PropertiesRouteDefinitionLocator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 动态路由配置，不从配置文件读取路由
 * @author 刘靖
 * @date 2020/6/4 16:41
 */
@Configuration
@ComponentScan("com.wosiliujing.learning.gateway")
public class DynamicRouteAutoConfiguration {

    @Bean
    public PropertiesRouteDefinitionLocator propertiesRouteDefinitionLocator() {
        return new PropertiesRouteDefinitionLocator(new GatewayProperties());
    }


}
