package com.wosiliujing.learning.gateway.event;

import org.springframework.context.ApplicationEvent;

/**
 * @author liujing
 * @Description: 路由初始化事件
 * @date 2019/6/3010:25
 */
public class DynamicRouteInitEvent extends ApplicationEvent {

    public DynamicRouteInitEvent(Object source) {
        super(source);
    }
}
