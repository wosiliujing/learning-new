package com.wosiliujing.learning.annotation;

import com.wosiliujing.learning.config.SwaggerAutoConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author 刘靖
 * @date 2019-10-14 10:30
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({SwaggerAutoConfiguration.class})
public @interface EnableLearningSwagger {

}
