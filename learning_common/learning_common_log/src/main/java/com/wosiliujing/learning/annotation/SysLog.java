package com.wosiliujing.learning.annotation;

import java.lang.annotation.*;

/**
 * @author: liujing
 * @date: 2019/5/17 17:25
 * @description: 系统日志注解
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SysLog {
    /**
     * 日志标题
     * @return 日志标题
     */
    String value();
}
