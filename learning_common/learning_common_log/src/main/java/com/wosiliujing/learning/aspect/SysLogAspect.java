package com.wosiliujing.learning.aspect;

import cn.hutool.json.JSONUtil;
import com.wosiliujing.learning.admin.SysLogDTO;
import com.wosiliujing.learning.annotation.SysLog;
import com.wosiliujing.learning.event.SysLogEvent;
import com.wosiliujing.learning.util.SpringContextHolder;
import com.wosiliujing.learning.util.SysLogUtil;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 * @author: liujing
 * @date: 2019/5/17 17:27
 * @description:
 */
@Aspect
@Slf4j
@Component
public class SysLogAspect {

    @Around("@annotation(sysLog)")
    public Object around(ProceedingJoinPoint point, SysLog sysLog) throws Throwable{
        String className = point.getTarget().getClass().getName();
        String methodName = point.getSignature().getName();
        log.info(sysLog.value()+"--类名:{},方法名:{}",className,methodName);
        SysLogDTO sysLogDto = SysLogUtil.getSysLog();
        sysLogDto.setTitle(sysLog.value());
        Long startTime = System.currentTimeMillis();
        Object obj = point.proceed();
        Long endTime = System.currentTimeMillis();
        sysLogDto.setTime(endTime-startTime);
        sysLogDto.setParams(JSONUtil.toJsonStr(point.getArgs()));
        SpringContextHolder.publishEvent(new SysLogEvent(sysLogDto));
        return obj;
    }
}
