package com.wosiliujing.learning.event;

import com.wosiliujing.learning.admin.SysLogDTO;
import org.springframework.context.ApplicationEvent;

/**
 * @author: liujing
 * @date: 2019/5/17 17:31
 * @description:
 */
public class SysLogEvent extends ApplicationEvent {


    public SysLogEvent(SysLogDTO  source) {
        super(source);
    }
}
