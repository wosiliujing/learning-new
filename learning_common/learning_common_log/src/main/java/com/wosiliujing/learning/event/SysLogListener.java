package com.wosiliujing.learning.event;

import com.wosiliujing.learning.admin.SysLogDTO;
import com.wosiliujing.learning.constant.SecurityConstants;
import com.wosiliujing.learning.service.RemoteSysLogService;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * @author: liujing
 * @date: 2019/5/17 18:08
 * @description:
 */
@Component
@AllArgsConstructor
public class SysLogListener implements ApplicationListener<SysLogEvent> {

    private final RemoteSysLogService remoteSysLogService;

    @Override
    public void onApplicationEvent(SysLogEvent event) {
        SysLogDTO sysLog = (SysLogDTO) event.getSource();
        remoteSysLogService.save(sysLog,SecurityConstants.FROM_IN);
    }
}
