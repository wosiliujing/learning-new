/*
 Navicat Premium Data Transfer

 Source Server         : 192.1.40.159
 Source Server Type    : MySQL
 Source Server Version : 80017
 Source Host           : 192.1.40.159:3306
 Source Schema         : learning

 Target Server Type    : MySQL
 Target Server Version : 80017
 File Encoding         : 65001

 Date: 05/06/2020 15:04:18
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '部门名称',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `parent_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标识',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES ('1', '测试根部门', 0, '2019-05-11 12:12:06', '2019-05-14 17:49:41', 'root', '0');
INSERT INTO `sys_dept` VALUES ('2', '测试子部门A', 1, '2019-05-11 12:13:00', '2020-06-04 16:07:18', '1', '0');
INSERT INTO `sys_dept` VALUES ('222d453fe7e43298a01480e13908d615', '测试部门2', 3, '2019-05-14 17:49:18', NULL, '1', '1');

-- ----------------------------
-- Table structure for sys_dept_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept_role`;
CREATE TABLE `sys_dept_role`  (
  `dept_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `role_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `label` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `sort` int(11) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('21f1c01cea1e5ac511426fedc3e4976e', '0', '测试标签1', '1', '测试数据', 0, '2019-05-19 16:10:26', NULL, '测试数据', '0');
INSERT INTO `sys_dict` VALUES ('69078d7a2da8550befd970c4dc1cb528', NULL, NULL, NULL, NULL, NULL, '2019-05-19 15:58:30', NULL, NULL, '1');
INSERT INTO `sys_dict` VALUES ('cf85b7990a2ee34d4f2b2daa50cda0cd', '234', '43234', '24', '424', 24, '2019-05-19 16:00:56', '2019-05-19 16:01:03', '24', '1');
INSERT INTO `sys_dict` VALUES ('d2a9ba423f170c4208e9b2947b576b43', NULL, NULL, NULL, NULL, NULL, '2019-05-19 15:59:46', NULL, NULL, '1');

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `type` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `user_agent` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `request_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `params` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `time` mediumint(9) NULL DEFAULT NULL,
  `client_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `exception` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `del_flag` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES ('31574c5fedaeb0402e84c404f0e00b98', NULL, '删除角色', 'liujing', '2020-06-04 16:11:56', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.100 Safari/537.36', '/role/delete/12', 'DELETE', '[\"12\"]', 13, 'test', NULL, '0');
INSERT INTO `sys_log` VALUES ('b28f9d9b3fb0c57347e77ece1d98ec34', NULL, '保存角色资源', 'liujing', '2020-06-05 14:56:43', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.100 Safari/537.36', '/role/resources', 'PUT', '[\"1\",\"1,2,240c1b0e33fc46bee24858f8261195ac,32538f108d04480e824e46f92cac7d65,70b916f633e0464711006821919ac961,a41c9292f7e1905ad2c311688301a51d,3,0ee608eff75ee32af8d7eb3642f73465,447b2ec73f32274bdc107df0a807e992,5554b1696b9065942c225a56deb86489,ccf6a3608297df1e701497bfb80088b1,4,3477e91706c7e0dfa1c4820c61b790b7,34f93e46d4e243434fb957d1358b2e6f,79456a653c5aa4cbdda7a7ce60038336,a501fc1a48e3e9fd3de79951864b26b6,b77d0bb99c5effb88f7ab14643d72e8b,7aa79be715f8eeafe066eced077d6db3,15a49cc4f6a9bf16b3636ce86c587d9c,2d80099867ec84ff0189c6b94e7daa81,348a046c712f8c752a51f13cd419b11a,3e91115a704f7b75e3cd34554dd0308b,71459eb18eaf05ff5b39ba513d6153f7,19a26649d536603794c7a6fd255fdfe2,2bbf1c3c29faaf7000a54fd4ddb598a4,6a02b5a50d1f69dfe9a40dc762871fad,a9cb83a5248b4a07930931dbd9635c41,7555abf1c5c5fab79f15e24ca4631bac,a4967e8d5387689dbb87274821e0d474,c1e12d40827c64ac9c2d0b2f0d460273,450874d692f9567a2824af4a11f389e8,4604f912e8980bd2eee2e2cefb009f9e,c895c6b3a85c238f0cd0b195ef4a8c7c,fed41319647471b1369dd0bda2f622af,c48c746bcbc0de51332073d794025164,0b3b4f02ef6269c1a8895b8c5d88981a,70b080ca8ba5636494cd45c4af018469,7aea1e0373471d70d976de2b981c6ca2,e178af43142bb076d5e924fa7774af77,22\"]', 262, 'test', NULL, '0');

-- ----------------------------
-- Table structure for sys_oauth_client_details
-- ----------------------------
DROP TABLE IF EXISTS `sys_oauth_client_details`;
CREATE TABLE `sys_oauth_client_details`  (
  `client_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `client_secret` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `resource_ids` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `scope` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `authorized_grant_types` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `web_server_redirect_uri` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `authorities` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `access_token_validity` int(11) NULL DEFAULT NULL,
  `refresh_token_validity` int(11) NULL DEFAULT NULL,
  `additional_information` varchar(4096) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `autoapprove` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`client_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_oauth_client_details
-- ----------------------------
INSERT INTO `sys_oauth_client_details` VALUES ('test', 'test', NULL, 'test', 'password,refresh_token,authorization_code,client_credentials', 'http://localhost:4040/sso/login,https://www.baidu.com,https://pig4cloud.com,http://localhost:2233/login', NULL, NULL, NULL, NULL, 'false');

-- ----------------------------
-- Table structure for sys_resources
-- ----------------------------
DROP TABLE IF EXISTS `sys_resources`;
CREATE TABLE `sys_resources`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '资源名称',
  `permission` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `parent_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图标',
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '路由',
  `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `keep_alive` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `type` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类型',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `del_flag` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '删除标识',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_resources
-- ----------------------------
INSERT INTO `sys_resources` VALUES ('03d7e640973863a299035f1d9de8d0e2', '新增', 'sys_activiti_model_add', 'c0204ba7de8492a71856406cd9931a25', '', '', '', '0', '1', 0, '2019-06-07 17:21:47', NULL, '0');
INSERT INTO `sys_resources` VALUES ('0b3b4f02ef6269c1a8895b8c5d88981a', '查看', 'sys_client_view', 'c48c746bcbc0de51332073d794025164', '', '', '', '0', '1', 0, '2019-05-23 15:29:25', NULL, '0');
INSERT INTO `sys_resources` VALUES ('0ee608eff75ee32af8d7eb3642f73465', '新增', 'sys_resources_add', '3', '', '', '', '0', '1', 0, '2019-05-23 14:41:47', NULL, '0');
INSERT INTO `sys_resources` VALUES ('1', '权限管理', 'sys_permission', 'root', NULL, '/admin', NULL, '0', '0', 0, '2019-05-13 16:45:22', '2019-10-21 17:09:16', '0');
INSERT INTO `sys_resources` VALUES ('15a49cc4f6a9bf16b3636ce86c587d9c', '查看', 'sys_dept_view', '7aa79be715f8eeafe066eced077d6db3', '', '', '', '0', '1', 0, '2019-05-23 15:14:16', NULL, '0');
INSERT INTO `sys_resources` VALUES ('15c3afa0ef2e79ccfa29f876cb7b39fe', '请假管理', 'sys_activiti_leave', '6', '', 'leave', 'views/activiti/leave', '0', '0', 0, '2019-06-07 17:38:58', NULL, '0');
INSERT INTO `sys_resources` VALUES ('19a26649d536603794c7a6fd255fdfe2', '查看', 'sys_dict_view', '71459eb18eaf05ff5b39ba513d6153f7', '', '', '', '0', '1', 0, '2019-05-23 15:26:35', NULL, '0');
INSERT INTO `sys_resources` VALUES ('1ddc69429db4be6756ee67059031b7f9', '管理', 'sys_activiti_task_manage', '72317f83a478b22b37f2cdfc46c92b84', '', '', '', '0', '1', 0, '2019-06-10 14:12:58', '2019-06-10 14:18:27', '0');
INSERT INTO `sys_resources` VALUES ('2', '用户管理', 'sys_user_index', '1', NULL, 'user', 'views/user/index', '0', '0', 2, '2019-05-09 14:46:38', NULL, '0');
INSERT INTO `sys_resources` VALUES ('22', '系统管理', 'sys_sys', 'root', NULL, '/sys', NULL, '0', '0', 1, '2019-05-19 13:04:24', NULL, '0');
INSERT INTO `sys_resources` VALUES ('240c1b0e33fc46bee24858f8261195ac', '新增', 'sys_user_add', '2', '', '', '', '0', '1', 0, '2019-05-23 13:56:28', NULL, '0');
INSERT INTO `sys_resources` VALUES ('2a62d93c24dc467633357f2959bd2fe9', '部署', 'sys_activiti_process_manage', '84e144346d04be6d619500eb8897736f', '', '', '', '0', '1', 0, '2019-06-07 17:38:11', NULL, '0');
INSERT INTO `sys_resources` VALUES ('2bbf1c3c29faaf7000a54fd4ddb598a4', '删除', 'sys_dict_del', '71459eb18eaf05ff5b39ba513d6153f7', '', '', '', '0', '1', 0, '2019-05-23 15:27:01', NULL, '0');
INSERT INTO `sys_resources` VALUES ('2d80099867ec84ff0189c6b94e7daa81', '删除', 'sys_dept_del', '7aa79be715f8eeafe066eced077d6db3', '', '', '', '0', '1', 0, '2019-05-23 15:13:48', NULL, '0');
INSERT INTO `sys_resources` VALUES ('3', '资源管理', 'sys_resources_index', '1', NULL, 'resources', 'views/resources/index', '0', '0', 3, '2019-05-09 16:04:05', NULL, '0');
INSERT INTO `sys_resources` VALUES ('32538f108d04480e824e46f92cac7d65', '编辑', 'sys_user_edit', '2', '', '', '', '0', '1', 2, '2019-05-23 14:16:02', '2019-05-23 14:17:06', '0');
INSERT INTO `sys_resources` VALUES ('3263371fa8a651886f1b7641554819a8', '部署', 'sys_activiti_model_deploy', 'c0204ba7de8492a71856406cd9931a25', '', '', '', '0', '1', 0, '2019-06-07 17:23:35', NULL, '0');
INSERT INTO `sys_resources` VALUES ('3477e91706c7e0dfa1c4820c61b790b7', '编辑', 'sys_role_edit', '4', '', '', '', '0', '1', 0, '2019-05-23 14:57:34', NULL, '0');
INSERT INTO `sys_resources` VALUES ('348a046c712f8c752a51f13cd419b11a', '新增', 'sys_dept_add', '7aa79be715f8eeafe066eced077d6db3', '', '', '', '0', '1', 0, '2019-05-23 15:12:46', NULL, '0');
INSERT INTO `sys_resources` VALUES ('34f93e46d4e243434fb957d1358b2e6f', '删除', 'sys_role_del', '4', '', '', '', '0', '1', 0, '2019-05-23 14:59:21', NULL, '0');
INSERT INTO `sys_resources` VALUES ('3e91115a704f7b75e3cd34554dd0308b', '编辑', 'sys_dept_edit', '7aa79be715f8eeafe066eced077d6db3', '', '', '', '0', '1', 0, '2019-05-23 15:13:26', NULL, '0');
INSERT INTO `sys_resources` VALUES ('3f68137ca1f2907953ce0b0cb4414576', '链路追踪', 'sys_zippin', '22', '', 'http://localhost:9006', '', '0', '0', 0, '2019-05-20 18:02:14', NULL, '0');
INSERT INTO `sys_resources` VALUES ('4', '角色管理', 'sys_roler_index', '1', NULL, 'role', 'views/role/index', '0', '0', 4, '2019-05-09 16:05:34', NULL, '0');
INSERT INTO `sys_resources` VALUES ('447b2ec73f32274bdc107df0a807e992', '查看', 'sys_resources_view', '3', '', '', '', '0', '1', 0, '2019-05-23 14:46:26', NULL, '0');
INSERT INTO `sys_resources` VALUES ('450874d692f9567a2824af4a11f389e8', '新增', 'sys_log_add', 'c1e12d40827c64ac9c2d0b2f0d460273', '', '', '', '0', '1', 0, '2019-05-23 15:23:56', NULL, '0');
INSERT INTO `sys_resources` VALUES ('4604f912e8980bd2eee2e2cefb009f9e', '查看', 'sys_log_view', 'c1e12d40827c64ac9c2d0b2f0d460273', '', '', '', '0', '1', 0, '2019-05-23 15:25:06', NULL, '0');
INSERT INTO `sys_resources` VALUES ('5554b1696b9065942c225a56deb86489', '删除', 'sys_resources_del', '3', '', '', '', '0', '1', 0, '2019-05-23 14:42:56', NULL, '0');
INSERT INTO `sys_resources` VALUES ('5b1fb79235df728a5818a85491a4dee4', '删除', 'sys_activiti_leave_del', '15c3afa0ef2e79ccfa29f876cb7b39fe', '', '', '', '0', '1', 0, '2019-06-10 10:02:43', NULL, '0');
INSERT INTO `sys_resources` VALUES ('6', '工作流', 'sys_activit', 'root', NULL, '/activiti', NULL, '0', '0', 0, '2019-06-07 17:19:30', '2019-06-07 17:24:24', '0');
INSERT INTO `sys_resources` VALUES ('652f58f9d41d7dfb42d8b825697e4df9', '新增', 'sys_activiti_leave_add', '15c3afa0ef2e79ccfa29f876cb7b39fe', '', '', '', '0', '1', 0, '2019-06-10 09:57:26', NULL, '0');
INSERT INTO `sys_resources` VALUES ('6a02b5a50d1f69dfe9a40dc762871fad', '编辑', 'sys_dict_edit', '71459eb18eaf05ff5b39ba513d6153f7', '', '', '', '0', '1', 0, '2019-05-23 15:26:11', NULL, '0');
INSERT INTO `sys_resources` VALUES ('70b080ca8ba5636494cd45c4af018469', '删除', 'sys_client_del', 'c48c746bcbc0de51332073d794025164', '', '', '', '0', '1', 0, '2019-05-23 15:28:56', NULL, '0');
INSERT INTO `sys_resources` VALUES ('70b916f633e0464711006821919ac961', '查看', 'sys_user_view', '2', '', '', '', '0', '1', 0, '2019-05-23 14:21:15', NULL, '0');
INSERT INTO `sys_resources` VALUES ('71459eb18eaf05ff5b39ba513d6153f7', '字典管理', 'sys_dict_index', '22', '', 'dict', 'views/dict/index', '0', '0', 7, '2019-05-19 15:34:52', '2019-05-19 15:35:04', '0');
INSERT INTO `sys_resources` VALUES ('72317f83a478b22b37f2cdfc46c92b84', '待办任务', 'sys_activiti_task', '6', '', 'task', 'views/activiti/task', '0', '0', 45, '2019-06-10 11:44:22', NULL, '0');
INSERT INTO `sys_resources` VALUES ('7555abf1c5c5fab79f15e24ca4631bac', '令牌管理', 'sys_token_index', '22', '', 'token', 'views/token/index', '0', '0', 10, '2019-05-19 19:14:40', NULL, '0');
INSERT INTO `sys_resources` VALUES ('76b3601ca7e9de1cbc0dc4dd8b836623', '服务监控', 'sys_monitor', '22', '', 'http://localhost:9005', '', '1', '0', 11, '2019-05-20 16:01:19', '2019-05-20 16:18:44', '0');
INSERT INTO `sys_resources` VALUES ('79456a653c5aa4cbdda7a7ce60038336', '权限', 'sys_role_permission', '4', '', '', '', '0', '1', 0, '2019-05-23 15:01:16', NULL, '0');
INSERT INTO `sys_resources` VALUES ('7aa79be715f8eeafe066eced077d6db3', '部门管理', 'sys_dept', '1', '', 'dept', 'views/dept/index', '0', '0', 5, '2019-05-14 17:26:56', NULL, '0');
INSERT INTO `sys_resources` VALUES ('7aea1e0373471d70d976de2b981c6ca2', '新增', 'sys_client_add', 'c48c746bcbc0de51332073d794025164', '', '', '', '0', '1', 0, '2019-05-23 15:27:41', NULL, '0');
INSERT INTO `sys_resources` VALUES ('84e144346d04be6d619500eb8897736f', '流程管理', 'sys_activiti_process', '6', '', 'process', 'views/activiti/process', '0', '0', 44, '2019-06-07 17:37:18', NULL, '0');
INSERT INTO `sys_resources` VALUES ('a41c9292f7e1905ad2c311688301a51d', '删除', 'sys_user_del', '2', '', '', '', '0', '1', 1, '2019-05-23 14:15:25', '2019-05-23 14:20:41', '0');
INSERT INTO `sys_resources` VALUES ('a4967e8d5387689dbb87274821e0d474', '路由管理', 'sys_route', '22', '', 'route', 'views/route/index', '0', '0', 0, '2019-09-17 09:49:02', '2019-09-17 09:51:44', '0');
INSERT INTO `sys_resources` VALUES ('a501fc1a48e3e9fd3de79951864b26b6', '查看', 'sys_role_view', '4', '', '', '', '0', '1', 0, '2019-05-23 15:00:02', NULL, '0');
INSERT INTO `sys_resources` VALUES ('a9cb83a5248b4a07930931dbd9635c41', '新增', 'sys_dict_add', '71459eb18eaf05ff5b39ba513d6153f7', '', '', '', '0', '1', 0, '2019-05-23 15:25:36', NULL, '0');
INSERT INTO `sys_resources` VALUES ('aec77a25a0d9e0a9f61978d8ee0cfa5d', '查看', 'sys_activiti_leave_view', '15c3afa0ef2e79ccfa29f876cb7b39fe', '', '', '', '0', '1', 0, '2019-06-10 10:01:03', NULL, '0');
INSERT INTO `sys_resources` VALUES ('b100d52fee46e8b41aea561f366b0198', 'nacos', 'sys_nacos', '22', '', 'http://192.1.40.15:8888/nacos/index.html#/login', '', '0', '0', 10, '2019-06-17 10:06:16', '2019-06-17 10:08:04', '1');
INSERT INTO `sys_resources` VALUES ('b30ed14e0a4b59fa252cfe1b82e928cd', '删除', 'sys_activiti_model_del', 'c0204ba7de8492a71856406cd9931a25', '', '', '', '0', '1', 0, '2019-06-07 17:23:06', NULL, '0');
INSERT INTO `sys_resources` VALUES ('b77d0bb99c5effb88f7ab14643d72e8b', '新增', 'sys_role_add', '4', '', '', '', '0', '1', 0, '2019-05-23 14:57:02', NULL, '0');
INSERT INTO `sys_resources` VALUES ('c0204ba7de8492a71856406cd9931a25', '模型管理', 'sys_activiti_model', '6', '', 'model', 'views/activiti/index', '0', '0', 43, '2019-06-07 17:20:45', NULL, '0');
INSERT INTO `sys_resources` VALUES ('c1e12d40827c64ac9c2d0b2f0d460273', '日志管理', 'sys_log_index', '22', '', 'log', 'views/log/index', '0', '0', 6, '2019-05-19 13:09:47', '2019-05-19 14:19:58', '0');
INSERT INTO `sys_resources` VALUES ('c48c746bcbc0de51332073d794025164', '客户端管理', 'sys_client_index', '22', '', 'client', 'views/client/index', '0', '0', 9, '2019-05-19 16:58:17', NULL, '0');
INSERT INTO `sys_resources` VALUES ('c72173ab5506927d420ba222b4e9cb42', '编辑', 'sys_activiti_model_edit', 'c0204ba7de8492a71856406cd9931a25', '', '', '', '0', '1', 0, '2019-06-07 17:22:17', NULL, '0');
INSERT INTO `sys_resources` VALUES ('c895c6b3a85c238f0cd0b195ef4a8c7c', '编辑', 'sys_log_edit', 'c1e12d40827c64ac9c2d0b2f0d460273', '', '', '', '0', '1', 0, '2019-05-23 15:24:21', NULL, '0');
INSERT INTO `sys_resources` VALUES ('ccf6a3608297df1e701497bfb80088b1', '编辑', 'sys_resources_edit', '3', '', '', '', '0', '1', 0, '2019-05-23 14:42:26', NULL, '0');
INSERT INTO `sys_resources` VALUES ('d7e787c389f5dc001fae0c52953502d4', '编辑', 'sys_activiti_leave_edit', '15c3afa0ef2e79ccfa29f876cb7b39fe', '', '', '', '0', '1', 0, '2019-06-10 10:01:59', NULL, '0');
INSERT INTO `sys_resources` VALUES ('dfa5eb491d522d5134e4ef0c1acd47c9', '查看', 'sys_activiti_model_view', 'c0204ba7de8492a71856406cd9931a25', '', '', '', '0', '1', 0, '2019-06-07 17:22:36', NULL, '0');
INSERT INTO `sys_resources` VALUES ('e178af43142bb076d5e924fa7774af77', '编辑', 'sys_client_edit', 'c48c746bcbc0de51332073d794025164', '', '', '', '0', '1', 0, '2019-05-23 15:28:25', NULL, '0');
INSERT INTO `sys_resources` VALUES ('fed41319647471b1369dd0bda2f622af', '删除', 'sys_log_del', 'c1e12d40827c64ac9c2d0b2f0d460273', '', '', '', '0', '1', 0, '2019-05-23 15:24:45', NULL, '0');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色编码',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色描述',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `del_flag` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '删除标识',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('0510131049c5a2ec64b5fe36dd4c5f00', '测试角色2', 'test02', '测试数据', '2019-05-13 16:05:37', '2019-05-13 16:07:21', '1');
INSERT INTO `sys_role` VALUES ('057c36da903bd682d3882badd8242a4b', '123123', '123123', '123123', '2019-05-13 15:56:28', NULL, '1');
INSERT INTO `sys_role` VALUES ('1', '超级管理员', 'root', '超级管理员', '2019-05-09 14:40:58', '2019-05-14 10:10:30', '0');
INSERT INTO `sys_role` VALUES ('12', '测试validator', '5252', NULL, '2019-10-12 14:27:27', NULL, '1');
INSERT INTO `sys_role` VALUES ('2', '测试管理员', 'test', '测试管理员', '2019-05-11 11:27:27', '2019-05-14 10:10:34', '0');
INSERT INTO `sys_role` VALUES ('597a452d145fd6d59102b0561e0333e8', '1312313', '13123', '1231231', '2019-05-13 15:54:43', NULL, '1');
INSERT INTO `sys_role` VALUES ('f7a52534ff18c35596f439732ace6161', '测试角色', 'test1', '测试数据', '2019-05-13 15:51:47', '2019-05-13 16:13:41', '1');

-- ----------------------------
-- Table structure for sys_role_resources
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_resources`;
CREATE TABLE `sys_role_resources`  (
  `role_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `resources_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_resources
-- ----------------------------
INSERT INTO `sys_role_resources` VALUES ('1', '240c1b0e33fc46bee24858f8261195ac', '047d55651e04b3663914f98275bd6ddd');
INSERT INTO `sys_role_resources` VALUES ('1', '19a26649d536603794c7a6fd255fdfe2', '08ce1133cedd9f3f4e61afaf992c79bb');
INSERT INTO `sys_role_resources` VALUES ('1', '5554b1696b9065942c225a56deb86489', '1e09e0550acdb96a7744abac60b01bbc');
INSERT INTO `sys_role_resources` VALUES ('1', 'a4967e8d5387689dbb87274821e0d474', '274ce72aae695dbbe508d310bfeb8f01');
INSERT INTO `sys_role_resources` VALUES ('1', 'c1e12d40827c64ac9c2d0b2f0d460273', '2b5cacf7373eb8a1fed2c1fa5f018315');
INSERT INTO `sys_role_resources` VALUES ('1', '15a49cc4f6a9bf16b3636ce86c587d9c', '2e727bc82b012e4a43ee4e02f64d636d');
INSERT INTO `sys_role_resources` VALUES ('1', '32538f108d04480e824e46f92cac7d65', '2f2c96eeedb58f5f046ef5fb4293fe69');
INSERT INTO `sys_role_resources` VALUES ('1', 'a41c9292f7e1905ad2c311688301a51d', '2f6b3e889dd1d05f4b6b4c175b5e5dd9');
INSERT INTO `sys_role_resources` VALUES ('1', '2d80099867ec84ff0189c6b94e7daa81', '355cb35c880e8fbaa9ef0ebdae47ec14');
INSERT INTO `sys_role_resources` VALUES ('1', '348a046c712f8c752a51f13cd419b11a', '39c465d44fa0d4339c8970fde6844275');
INSERT INTO `sys_role_resources` VALUES ('1', '7555abf1c5c5fab79f15e24ca4631bac', '3aeafc4479e9137e874b7cd6ba9db223');
INSERT INTO `sys_role_resources` VALUES ('1', 'b77d0bb99c5effb88f7ab14643d72e8b', '476438bb6f55be636ec96453f4ba0a3d');
INSERT INTO `sys_role_resources` VALUES ('1', '34f93e46d4e243434fb957d1358b2e6f', '4b713cce8ae14fdeb564b846a588bb50');
INSERT INTO `sys_role_resources` VALUES ('1', 'e178af43142bb076d5e924fa7774af77', '4d2be2319b736aee31ac0fac6fd0242a');
INSERT INTO `sys_role_resources` VALUES ('1', 'c895c6b3a85c238f0cd0b195ef4a8c7c', '5a3327b1a23b55b9ae097d2e40aca620');
INSERT INTO `sys_role_resources` VALUES ('1', '71459eb18eaf05ff5b39ba513d6153f7', '60b6456b1ece0f44b03c1b07d5c97b71');
INSERT INTO `sys_role_resources` VALUES ('1', '450874d692f9567a2824af4a11f389e8', '62fb16360e85f32ed2c333681160f5d1');
INSERT INTO `sys_role_resources` VALUES ('1', '6a02b5a50d1f69dfe9a40dc762871fad', '8446e8a24b19620ee2a2df75962db21c');
INSERT INTO `sys_role_resources` VALUES ('1', '2', '84c4acd16c9f36ab4ad6d20c6ce729e0');
INSERT INTO `sys_role_resources` VALUES ('1', '7aa79be715f8eeafe066eced077d6db3', '93e988026534922c92720f90cd4ff806');
INSERT INTO `sys_role_resources` VALUES ('1', '79456a653c5aa4cbdda7a7ce60038336', '95cd6f7e663ce8c6a538ba86b6e559bc');
INSERT INTO `sys_role_resources` VALUES ('1', 'fed41319647471b1369dd0bda2f622af', '9f1c5c38eccbeebeed117698de774d43');
INSERT INTO `sys_role_resources` VALUES ('1', '2bbf1c3c29faaf7000a54fd4ddb598a4', '9f65afb381b2e17b04aea9197cc1cad3');
INSERT INTO `sys_role_resources` VALUES ('1', '0b3b4f02ef6269c1a8895b8c5d88981a', 'a17993c6d0be37716f3a43479ecee39f');
INSERT INTO `sys_role_resources` VALUES ('1', '3e91115a704f7b75e3cd34554dd0308b', 'a1da90ec97681025ae187c802424400b');
INSERT INTO `sys_role_resources` VALUES ('1', '3477e91706c7e0dfa1c4820c61b790b7', 'a2ee157a20a2733c79c874e150432ba4');
INSERT INTO `sys_role_resources` VALUES ('1', 'a9cb83a5248b4a07930931dbd9635c41', 'adedc3cc805cd2b48b324a19545b8e7a');
INSERT INTO `sys_role_resources` VALUES ('1', '1', 'b5cb9407558428ea48c9f2cbbe0355a7');
INSERT INTO `sys_role_resources` VALUES ('1', 'c48c746bcbc0de51332073d794025164', 'c46553b1dcf262a48dc3944a85952252');
INSERT INTO `sys_role_resources` VALUES ('1', '0ee608eff75ee32af8d7eb3642f73465', 'cc7a0f6c93041189a7fb192c16c48d5a');
INSERT INTO `sys_role_resources` VALUES ('1', '4', 'cd4eff3d0d31f300d24d45552a64a81b');
INSERT INTO `sys_role_resources` VALUES ('1', '70b916f633e0464711006821919ac961', 'db68aba543e642b6954bd7b9530c69a4');
INSERT INTO `sys_role_resources` VALUES ('1', '4604f912e8980bd2eee2e2cefb009f9e', 'dee385b6cb777edd76fd547f196bae8f');
INSERT INTO `sys_role_resources` VALUES ('1', 'ccf6a3608297df1e701497bfb80088b1', 'df244f4c6f8e2e6a8f64c7914fbeddcd');
INSERT INTO `sys_role_resources` VALUES ('1', '447b2ec73f32274bdc107df0a807e992', 'e061db44049b5f863c24fd76abca105b');
INSERT INTO `sys_role_resources` VALUES ('1', 'a501fc1a48e3e9fd3de79951864b26b6', 'e65bec25057cd7452c0cec08f3eefff9');
INSERT INTO `sys_role_resources` VALUES ('1', '22', 'e882a2b614b630742a301ec901cbbc00');
INSERT INTO `sys_role_resources` VALUES ('1', '70b080ca8ba5636494cd45c4af018469', 'edf850f9b65d2e95e999f4f6a3478b4f');
INSERT INTO `sys_role_resources` VALUES ('1', '3', 'efd6979a2e74c3b1a371e2731775730f');
INSERT INTO `sys_role_resources` VALUES ('1', '7aea1e0373471d70d976de2b981c6ca2', 'f81b3bb8e15ca48da5f4596ff2fed01b');

-- ----------------------------
-- Table structure for sys_route_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_route_config`;
CREATE TABLE `sys_route_config`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'id',
  `route_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '路由id',
  `route_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '路由名称',
  `predicates` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '断言',
  `filters` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '过滤器',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'url',
  `order` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `del_flag` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '删除标识',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_route_config
-- ----------------------------
INSERT INTO `sys_route_config` VALUES ('232a56482a5372748fb01fc957b00c9e', 'learning-activiti', 'activity', '[{\"args\":{\"_genkey_0\":\"/activiti/**\"},\"name\":\"Path\"}]', '[{\"args\":{\"_genkey_0\":\"/activiti/(?<segment>.*)\",\"_genkey_1\":\"/$\\\\{segment}\"},\"name\":\"RewritePath\"},{\"args\":{\"fallbackUri\":\"forward:/fallback\",\"name\":\"default\"},\"name\":\"Hystrix\"}]', 'lb://learning-activiti', 0, '2019-09-17 11:13:30', NULL, '0');
INSERT INTO `sys_route_config` VALUES ('cfa3d0c7a078a19be1a9cbc36d28597b', 'learning-auth', 'auth认证中心', '[{\"args\":{\"_genkey_0\":\"/auth/**\"},\"name\":\"Path\"}]', '[{\"args\":{\"_genkey_0\":\"/auth/(?<segment>.*)\",\"_genkey_1\":\"/$\\\\{segment}\"},\"name\":\"RewritePath\"}]', 'lb://learning-auth', 0, '2019-09-17 11:13:30', NULL, '0');
INSERT INTO `sys_route_config` VALUES ('ecfb1cc7d778ae914a8b9a70994608d1', 'learning-admin', '后台管理', '[{\"args\":{\"_genkey_0\":\"/admin/**\"},\"name\":\"Path\"}]', '[{\"args\":{\"_genkey_0\":\"/admin/(?<segment>.*)\",\"_genkey_1\":\"/$\\\\{segment}\"},\"name\":\"RewritePath\"},{\"args\":{\"fallbackUri\":\"forward:/fallback\",\"name\":\"default\"},\"name\":\"Hystrix\"}]', 'lb://learning-admin', 0, '2019-09-17 11:13:30', NULL, '0');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `user_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `dept_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `del_flag` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '删除标识',
  `lock_flag` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '锁定标识',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'liujing', '$2a$10$t15RAvBxMdO9OmPuux4Qk.woA6fEtfndX.7CoXWZ3GbviUOaOyPLa', '18819457418', '2', '2019-05-09 14:40:01', '2019-05-23 14:25:52', '0', '0', 'https://gitee.com/uploads/61/632261_smallweigit.jpg');
INSERT INTO `sys_user` VALUES ('2', 'test', '$2a$10$t15RAvBxMdO9OmPuux4Qk.woA6fEtfndX.7CoXWZ3GbviUOaOyPLa', '122112121', '1', '2019-05-10 15:04:21', '2019-05-15 16:35:05', '1', '0', NULL);
INSERT INTO `sys_user` VALUES ('29591b1eb98e968a58da1465d28a6db9', '测试12', '$2a$10$Qm/aA9tDdLl8sh3CdLKV6edXdCxYdtTE5wfbOp2Tg/bm3VvumCfR2', '', '2', '2019-05-17 17:45:49', NULL, '1', '0', NULL);
INSERT INTO `sys_user` VALUES ('371838145cf8fb84d04032fbc9124b01', 'test2', '$2a$10$3BQHHLeqJkxpfnc6T9Y9neg1kOtK.ROcMiSgBadGgw6MNjy2V9RJy', '', '2', '2019-06-10 11:40:20', NULL, '1', '0', NULL);
INSERT INTO `sys_user` VALUES ('78c977a06274e3023d9ccecbbd853e64', '测试用户2', '$2a$10$L8SJy84YVOc0N.FyTOdz8uPRsacuM2XiG/j1c78dKlr7ncKrHjkCa', '', '2', '2019-05-17 17:38:21', NULL, '1', '0', NULL);
INSERT INTO `sys_user` VALUES ('7e7d31bfef82052aa083cead8fb8eebc', '测试用户', '$2a$10$Mn1/UL69pMrg.iR86hhS2uiIZnJtJO9b3y9Lj9XIGSTd.Nd.Lzjxa', '', '2', '2019-05-17 15:51:33', NULL, '1', '0', NULL);
INSERT INTO `sys_user` VALUES ('e01c1208ff367363e7919c7563d467c8', '测试用户', '$2a$10$hjN5OU6l458WYP.OlVlYLOpF8DOSHBMnAkBwnbiwUps7CXpApkwcq', '', '2', '2019-05-17 17:40:05', NULL, '1', '0', NULL);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `role_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1', '2');

SET FOREIGN_KEY_CHECKS = 1;
