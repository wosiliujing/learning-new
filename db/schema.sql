-- learning 核心表
create database `learning` default character set utf8mb4 collate utf8mb4_general_ci;

-- learning 配置中心
create database `learning_nacos` default character set utf8mb4 collate utf8mb4_general_ci;

-- learning 工作流
create database `actest` default character set utf8mb4 collate utf8mb4_general_ci;

-- learning xxl-job调度中心
create database `xxl` default character set utf8mb4 collate utf8mb4_general_ci;