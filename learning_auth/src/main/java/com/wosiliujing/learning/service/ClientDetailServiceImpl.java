package com.wosiliujing.learning.service;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.oauth2.common.exceptions.InvalidClientException;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

/**
 * @author liujing
 */
@Component
public class ClientDetailServiceImpl extends JdbcClientDetailsService {

    public ClientDetailServiceImpl(DataSource dataSource){
        super(dataSource);
    }

    @Override
    @Cacheable(value = "learning_client_detail#50", key = "#clientId" ,unless = "#result == null")
    public ClientDetails loadClientByClientId(String clientId) throws InvalidClientException {
        return super.loadClientByClientId(clientId);
    }


}
