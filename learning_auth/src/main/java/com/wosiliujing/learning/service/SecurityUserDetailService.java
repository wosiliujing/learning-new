package com.wosiliujing.learning.service;

import com.wosiliujing.learning.validator.SocialParams;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

/***
 * 拓展获取登陆用户信息方式
 * @author 刘靖
 * @date 2019/9/23 10:03
 */
public interface SecurityUserDetailService extends UserDetailsService {

    /***
     * 社交方式登陆
     * @author 刘靖
     * @date 2019/9/23 10:04
     * @param socialParams 登陆参数
     * @return 用户信息
     */
    UserDetails loadUserBySocial(SocialParams socialParams);
}
