package com.wosiliujing.learning.service;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.wosiliujing.learning.constant.CommonConstants;
import com.wosiliujing.learning.constant.SecurityConstants;
import com.wosiliujing.learning.entity.SecurityUser;
import com.wosiliujing.learning.entity.UserInfoDTO;
import com.wosiliujing.learning.util.RestResult;
import com.wosiliujing.learning.validator.SocialParams;
import lombok.AllArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/****
 *
 * @author 刘靖
 * @date 2019/9/24 9:59
 */
@Component
@AllArgsConstructor
public class SecurityUserDetailServiceImpl implements SecurityUserDetailService {

    private final RemoteSysUserService remoteUserService;

    private final RemoteSysSocialDetailsService remoteSysSocialDetailsService;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        RestResult<UserInfoDTO> result = remoteUserService.info(s,SecurityConstants.FROM_IN);
        return getUserDetails(result);
    }

    @Override
    public UserDetails loadUserBySocial(SocialParams socialParams) {
        RestResult<UserInfoDTO> result = remoteSysSocialDetailsService.socialLogin(socialParams.getType(),socialParams.getCode(),SecurityConstants.FROM_IN);
        return getUserDetails(result);
    }

    /***
     * 获取security用户信息
     * @author 刘靖
     * @date 2019/9/23 10:15
     * @param userResult 用户信息
     * @return  获取security信息
     */
    private UserDetails getUserDetails(RestResult<UserInfoDTO>  userResult){
        if(ObjectUtil.isEmpty(userResult)|| ObjectUtil.isEmpty(userResult.getData())){
            if(userResult.getCode() == CommonConstants.FAIL){
                throw new InvalidGrantException( userResult.getMsg());
            }
            throw new InvalidGrantException(userResult.getMsg());
        }
        UserInfoDTO userInfo = userResult.getData();
        if(CollectionUtil.isEmpty(userInfo.getPermission())){
            throw new InvalidGrantException("用户暂无权限");
        }
        Set<String> authSet = new HashSet<>(userInfo.getPermission());
        Collection<? extends GrantedAuthority> authorities
                = AuthorityUtils.createAuthorityList(authSet.toArray(new String[0]));
        return new SecurityUser(userInfo.getEnhanceMap(),userInfo.getUserName(),SecurityConstants.PASSWORD_ENCODER_TYPE+userInfo.getPassword()
                ,true,true,true,true,authorities);
    }
}
