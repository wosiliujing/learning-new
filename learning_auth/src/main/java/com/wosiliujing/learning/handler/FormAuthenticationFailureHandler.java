package com.wosiliujing.learning.handler;

import cn.hutool.http.HttpUtil;
import com.wosiliujing.learning.util.WebUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.Charset;

/**
 * @author 刘靖
 * @date 2019-10-11 11:40
 */
@Slf4j
public class FormAuthenticationFailureHandler  implements AuthenticationFailureHandler {
    @Override
    public void onAuthenticationFailure(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        log.debug("表单登录失败:{}", e.getLocalizedMessage());
        WebUtils.getResponse().sendRedirect(String.format("/token/login?error=%s"
                , HttpUtil.encodeParams(e.getMessage(), Charset.defaultCharset())));
    }
}
