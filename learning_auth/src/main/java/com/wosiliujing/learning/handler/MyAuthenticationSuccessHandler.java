package com.wosiliujing.learning.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

/**
 * @author 刘靖
 * @date 2019-09-25 14:47
 */
@Slf4j
@Component
public class MyAuthenticationSuccessHandler extends LearningAuthenticationSuccessHandler {

    @Override
    public void handle(Authentication authentication) {
        log.info("用户：{} 登录成功", authentication.getPrincipal());
    }
}
