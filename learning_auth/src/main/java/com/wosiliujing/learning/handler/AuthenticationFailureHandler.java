package com.wosiliujing.learning.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AbstractAuthenticationFailureEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;


/**
 * @author liujing
 */
@Slf4j
@Component
public class AuthenticationFailureHandler  implements ApplicationListener<AbstractAuthenticationFailureEvent> {
    @Override
    public void onApplicationEvent(AbstractAuthenticationFailureEvent abstractAuthenticationFailureEvent) {
        AuthenticationException  authenticationException = abstractAuthenticationFailureEvent.getException();
        Authentication authentication = (Authentication)abstractAuthenticationFailureEvent.getSource();
        log.info("用户：{} 登录失败，异常：{}", authentication.getPrincipal(), authenticationException.getLocalizedMessage());
        abstractAuthenticationFailureEvent.getException().printStackTrace();
    }


}
