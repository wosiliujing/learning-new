package com.wosiliujing.learning.mobile;

import com.alibaba.fastjson.JSON;
import com.wosiliujing.learning.validator.SocialParams;
import com.wosiliujing.learning.constant.CommonConstants;
import com.wosiliujing.learning.constant.LoginTypeConstants;
import com.wosiliujing.learning.constant.SecurityConstants;
import com.wosiliujing.learning.util.RestResult;
import com.wosiliujing.learning.validator.SocialValidator;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationEventPublisher;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author 刘靖
 * @date 2019-09-24 14:51
 */
public class MobileAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    @Getter
    @Setter
    private boolean postOnly = true;

    @Getter
    @Setter
    private AuthenticationEventPublisher eventPublisher;

    @Getter
    @Setter
    private AuthenticationEntryPoint authenticationEntryPoint;

    @Getter
    @Setter
    private SocialValidator socialValidator ;


    public MobileAuthenticationFilter() {
        super(new AntPathRequestMatcher(SecurityConstants.MOBILE_TOKEN_URL, "POST"));
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        Authentication authResult = null;
        try{
            if (postOnly && !request.getMethod().equals(HttpMethod.POST.name())) {
                throw new AuthenticationServiceException(
                        "Authentication method not supported: " + request.getMethod());
            }
            SocialParams socialParams = obtainSocial(request);
            if(socialParams.getType().equals(LoginTypeConstants.SMS)){
               RestResult<String> result = socialValidator.valid(socialParams.getCode(),request.getParameter(LoginTypeConstants.RAM_STR));
               if(result.getCode() != CommonConstants.SUCCESS){
                   throw new InvalidGrantException(result.getMsg());
               }
            }
            MobileAuthenticationToken mobileAuthenticationToken = new MobileAuthenticationToken(JSON.toJSON(socialParams).toString());
            setDetails(request,mobileAuthenticationToken);
            authResult = this.getAuthenticationManager().authenticate(mobileAuthenticationToken);
            logger.debug("Authentication success: " + authResult);
            SecurityContextHolder.getContext().setAuthentication(authResult);
        }catch (Exception failed){
            SecurityContextHolder.clearContext();
            logger.debug("Authentication request failed: " + failed);

            eventPublisher.publishAuthenticationFailure(new BadCredentialsException(failed.getMessage(), failed),
                    new PreAuthenticatedAuthenticationToken("access-token", "N/A"));
            try {
                authenticationEntryPoint.commence(request, response,
                        new UsernameNotFoundException(failed.getMessage(), failed));
            } catch (Exception e) {
                logger.error("authenticationEntryPoint handle error:{}", failed);
            }
        }
        return authResult;
    }

    private SocialParams obtainSocial(HttpServletRequest request)  {
        SocialParams socialParams = new SocialParams();
        socialParams.setType(request.getParameter(LoginTypeConstants.TYPE));
        socialParams.setCode(request.getParameter(LoginTypeConstants.CODE));
        return socialParams;
    }

    private void setDetails(HttpServletRequest request,
                            MobileAuthenticationToken authRequest) {
        authRequest.setDetails(authenticationDetailsSource.buildDetails(request));
    }
}
