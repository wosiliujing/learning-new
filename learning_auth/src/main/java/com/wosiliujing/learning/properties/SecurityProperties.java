package com.wosiliujing.learning.properties;

import lombok.Data;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

/**
 * 权限属性配置
 * @author 刘靖
 * @date 2019-12-12 14:33
 */
@Data
@Configuration
@RefreshScope
@ConditionalOnExpression("!'${auth}'.isEmpty()")
@ConfigurationProperties(prefix = "auth")
public class SecurityProperties {

    /**
     * token有效时间 默认1小时
     */
    private Integer tokenValidTime = 60*60;

    /**
     * refresh token 有效时间
     */
    private Integer refreshTokenValidTime = 10*60*60;
}
