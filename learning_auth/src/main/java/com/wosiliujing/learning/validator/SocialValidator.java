package com.wosiliujing.learning.validator;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.wosiliujing.learning.constant.CommonConstants;
import com.wosiliujing.learning.constant.LoginTypeConstants;
import com.wosiliujing.learning.util.RestResult;
import lombok.AllArgsConstructor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

/**
 * @author 刘靖
 * @date 2019-09-26 11:13
 */
@Component
@AllArgsConstructor
public class SocialValidator {

    private final RedisTemplate redisTemplate;

    public RestResult<String> valid(String mobile, String code) {
        if(StrUtil.isBlank(mobile)){
            return  RestResult.error("手机号不能为空");
        }
        if(StrUtil.isBlank(code)) {
            return RestResult.error("验证码不能为空");
        }
        Object redisCode = redisTemplate.opsForValue().get(CommonConstants.SMS_CODE_KEY_ + LoginTypeConstants.SMS + "@" + mobile);
        if(ObjectUtil.isEmpty(redisCode)){
            return  RestResult.error("验证码错误");
        }
        if(!redisCode.equals(code)){
            return  RestResult.error("验证码错误");
        }
        return RestResult.ok();
    }
}
