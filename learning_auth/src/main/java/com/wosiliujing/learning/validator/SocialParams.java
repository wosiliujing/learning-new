package com.wosiliujing.learning.validator;

import lombok.Data;

/***
 * 社交登陆
 * @author 刘靖
 * @date 2019/9/23 10:07
 */
@Data
public class SocialParams {
    /**
     * 登陆方式
     */
    private String type;
    /**
     * 登陆标识码
     */
    private String code;
}
