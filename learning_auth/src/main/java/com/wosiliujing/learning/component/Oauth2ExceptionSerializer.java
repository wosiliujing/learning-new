package com.wosiliujing.learning.component;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.wosiliujing.learning.exception.LearningOauth2Exception;

import java.io.IOException;

/**
 * @author: liujing
 * @date: 2019/5/15 22:45
 * @description: 异常格式化
 */
public class Oauth2ExceptionSerializer extends StdSerializer<LearningOauth2Exception>{

    public Oauth2ExceptionSerializer(){
        super(LearningOauth2Exception.class);
    }

    @Override
    public void serialize(LearningOauth2Exception value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeStartObject();
        gen.writeObjectField("code", value.getHttpErrorCode());
        gen.writeStringField("msg", value.getMessage());
        if (value.getAdditionalInformation()!=null) {
            gen.writeObjectField("data", JSONObject.toJSON(value.getAdditionalInformation()));
        }
        gen.writeEndObject();
    }
}