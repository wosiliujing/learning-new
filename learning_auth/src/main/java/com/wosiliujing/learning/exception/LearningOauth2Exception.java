package com.wosiliujing.learning.exception;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.wosiliujing.learning.component.Oauth2ExceptionSerializer;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;

/**
 * @author: liujing
 * @date: 2019/5/15 22:48
 * @description: 自定义异常
 */
@JsonSerialize(using = Oauth2ExceptionSerializer.class)
public class LearningOauth2Exception extends OAuth2Exception {

    public LearningOauth2Exception(String msg, Throwable t) {
        super(msg, t);
    }

    public LearningOauth2Exception(String msg){
        super(msg);
    }

}
