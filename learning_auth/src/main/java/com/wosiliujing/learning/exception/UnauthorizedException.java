package com.wosiliujing.learning.exception;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.wosiliujing.learning.component.Oauth2ExceptionSerializer;
import org.springframework.http.HttpStatus;

/**
 * @author: liujing
 * @date: 2019/5/15 22:37
 * @description:
 */
@JsonSerialize(using = Oauth2ExceptionSerializer.class)
public class UnauthorizedException extends LearningOauth2Exception{

    public UnauthorizedException(String msg,Throwable t){
        super(msg,t);
    }

    @Override
    public String getOAuth2ErrorCode() {
        return HttpStatus.UNAUTHORIZED.getReasonPhrase();
    }

    @Override
    public int getHttpErrorCode() {
        return HttpStatus.UNAUTHORIZED.value();
    }

}
