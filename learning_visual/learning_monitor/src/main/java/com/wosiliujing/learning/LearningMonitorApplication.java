package com.wosiliujing.learning;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;


@EnableAdminServer
@SpringCloudApplication
public class LearningMonitorApplication {

    public static void main(String[] args) {
        SpringApplication.run(LearningMonitorApplication.class, args);
    }

}
