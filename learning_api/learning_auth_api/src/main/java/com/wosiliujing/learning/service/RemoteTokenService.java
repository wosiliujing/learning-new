package com.wosiliujing.learning.service;

import com.wosiliujing.learning.constant.MicroServiceNameConstants;
import com.wosiliujing.learning.fallback.RemoteTokenServiceFallbackImpl;
import com.wosiliujing.learning.util.RestResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author: liujing
 * @date: 2019/5/19 18:38
 * @description:
 */
@FeignClient(value = MicroServiceNameConstants.LEARNING_AUTH,fallback = RemoteTokenServiceFallbackImpl.class,contextId = "remoteTokenService")
public interface RemoteTokenService {

    /**
     * 删除token
     * @param token token
     * @return 删除结果
     */
    @DeleteMapping("/token/{token}")
    RestResult deleteToken(@PathVariable(value = "token") String token);

    /**
     *  分页数据
     * @param params 分页参数
     * @return 分页结果
     */
    @GetMapping("/token/page")
    RestResult getTokenPage(@RequestParam Map<String, Object> params);
}
