package com.wosiliujing.learning.fallback;

import com.wosiliujing.learning.service.RemoteTokenService;
import com.wosiliujing.learning.util.RestResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author: liujing
 * @date: 2019/5/19 18:40
 * @description:
 */
@Slf4j
@Component
public class RemoteTokenServiceFallbackImpl implements RemoteTokenService {
    @Override
    public RestResult deleteToken(String token) {
        log.info("删除token失败");
        return null;
    }

    @Override
    public RestResult getTokenPage(Map<String, Object> params) {
        log.info("获取token失败");
        return null;
    }
}
