package com.wosiliujing.learning.admin;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author 刘靖
 * @date 2020-06-05 11:32
 */
@Data
public class SysLogDTO {

    /**
     * 日志类型
     */
    private String type;

    /**
     * 标题
     */
    private String title;

    /**
     * 操作人
     */
    private String createUserName;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 操作者ip地址
     */
    private String ip;

    /**
     * 用户代理
     */
    private String userAgent;

    /**
     * 请求地址
     */
    private String  requestUrl;

    /**
     * 请求方法类型
     */
    private String method;

    /**
     * 请求参数
     */
    private String params;

    /**
     * 执行时间
     */
    private Long time;

    /**
     * 异常信息
     */
    private String exception;

    /**
     * 客户端id
     */
    private String clientId;

    /**
     * 删除标识
     */
    private String delFlag;
}
