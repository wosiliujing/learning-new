package com.wosiliujing.learning.admin;

import com.wosiliujing.learning.group.Insert;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author: liujing
 * @date: 2019/5/8 14:45
 * @description:
 */
@Data
public class RoleDTO implements Serializable {

    /**
     * id
     */
    @NotBlank(message = "id不能为空", groups = Insert.class)
    private String id;

    /**
     * 角色名称
     */
    @NotBlank(message = "角色名称不能为空")
    private String name;

    /**
     * 角色编码
     */
    @NotBlank(message = "角色编码不能为空")
    private String code;

    /**
     * 角色描述
     */
    private String description;

}
