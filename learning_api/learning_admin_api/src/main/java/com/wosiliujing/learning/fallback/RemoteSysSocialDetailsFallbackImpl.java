package com.wosiliujing.learning.fallback;

import com.wosiliujing.learning.service.RemoteSysSocialDetailsService;
import com.wosiliujing.learning.util.RestResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author 刘靖
 * @date 2019-09-24 9:48
 */
@Slf4j
@Component
public class RemoteSysSocialDetailsFallbackImpl implements RemoteSysSocialDetailsService {

    @Override
    public RestResult socialLogin(String type, String code, String from) {
        log.error("feign获取用户信息失败:{}",code);
        return null;
    }
}
