package com.wosiliujing.learning.fallback;

import com.wosiliujing.learning.admin.SysLogDTO;
import com.wosiliujing.learning.service.RemoteSysLogService;
import com.wosiliujing.learning.util.RestResult;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author: liujing
 * @date: 2019/5/17 11:15
 * @description:
 */
@Slf4j
@Component
public class RemoteSysLogFallbackImpl implements RemoteSysLogService {
    @Setter
    private Throwable cause;

    @Override
    public RestResult save(SysLogDTO sysLogDto, String from) {
        log.error("feign 插入日志失败", cause);
        return  null;
    }
}
