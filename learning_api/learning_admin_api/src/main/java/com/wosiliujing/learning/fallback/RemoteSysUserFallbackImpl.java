package com.wosiliujing.learning.fallback;

import com.wosiliujing.learning.entity.UserInfoDTO;
import com.wosiliujing.learning.service.RemoteSysUserService;
import com.wosiliujing.learning.util.RestResult;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author: liujing
 * @date: 2019/5/4 17:22
 * @description:
 */
@Slf4j
@Component
public class RemoteSysUserFallbackImpl implements FallbackFactory<RemoteSysUserService> {

    @Override
    public RemoteSysUserService create(Throwable throwable) {
        return new RemoteSysUserService() {
            @Override
            public RestResult<UserInfoDTO> info(String userName, String from) {
                log.error("feign获取用户信息失败:{}",userName);
                throwable.printStackTrace();
                return RestResult.error();
            }

            @Override
            public RestResult listAll(String from) {
                log.error("feign获取用户列表失败");
                throwable.printStackTrace();
                return RestResult.error();
            }
        };
    }
}
