package com.wosiliujing.learning.service;

import com.wosiliujing.learning.constant.MicroServiceNameConstants;
import com.wosiliujing.learning.constant.SecurityConstants;
import com.wosiliujing.learning.entity.UserInfoDTO;
import com.wosiliujing.learning.fallback.RemoteSysSocialDetailsFallbackImpl;
import com.wosiliujing.learning.util.RestResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

/**
 * 社交登陆 接口
 * @author 刘靖
 * @date 2019-09-24 9:43
 */
@FeignClient(value = MicroServiceNameConstants.LEARNING_ADMIN,fallback = RemoteSysSocialDetailsFallbackImpl.class,contextId = "socialService")
public interface RemoteSysSocialDetailsService {

    /***
     * 社交登陆用户信息获取
     * @author 刘靖
     * @date 2019/9/24 9:40
     * @param type 社交类型
     * @param code  身份标识
     * @return 用户信息
     */
    @GetMapping("/social/info/{type}/{code}")
    RestResult<UserInfoDTO> socialLogin(@PathVariable("type") String type, @PathVariable("code") String code, @RequestHeader(SecurityConstants.FROM) String from);
}
