package com.wosiliujing.learning.service;

import com.wosiliujing.learning.admin.SysLogDTO;
import com.wosiliujing.learning.constant.MicroServiceNameConstants;
import com.wosiliujing.learning.constant.SecurityConstants;
import com.wosiliujing.learning.fallback.RemoteSysLogFallbackImpl;
import com.wosiliujing.learning.util.RestResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

/**
 * @author: liujing
 * @date: 2019/5/17 11:12
 * @description:
 */
@FeignClient(value = MicroServiceNameConstants.LEARNING_ADMIN, fallback = RemoteSysLogFallbackImpl.class,contextId = "remoteSysLogService")
public interface RemoteSysLogService {

    /**
     * 保存系统日志
     * @param sysLogDto
     * @return
     */
    @PostMapping("/log/save")
    RestResult save(@RequestBody SysLogDTO sysLogDto, @RequestHeader(SecurityConstants.FROM) String from);
}
