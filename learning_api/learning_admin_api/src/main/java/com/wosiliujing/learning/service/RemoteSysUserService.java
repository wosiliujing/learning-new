package com.wosiliujing.learning.service;

import com.wosiliujing.learning.constant.MicroServiceNameConstants;
import com.wosiliujing.learning.constant.SecurityConstants;
import com.wosiliujing.learning.entity.UserInfoDTO;
import com.wosiliujing.learning.fallback.RemoteSysUserFallbackImpl;
import com.wosiliujing.learning.util.RestResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author liujing
 */
@FeignClient(value = MicroServiceNameConstants.LEARNING_ADMIN, fallbackFactory = RemoteSysUserFallbackImpl.class,contextId ="remoteSysUserService" )
public interface RemoteSysUserService {

    /**
     * 通过用户名获取用户信息
     * @param userName
     * @return
     */
    @RequestMapping("/user/info/{userName}")
    RestResult<UserInfoDTO> info(@PathVariable("userName") String userName, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 获取所有用户信息
     * @return 用户信息列表
     */
    @GetMapping("/user/listAll")
    RestResult listAll(@RequestHeader(SecurityConstants.FROM) String from);
}
