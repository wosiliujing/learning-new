package com.wosiliujing.learning.social.mapppr;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wosiliujing.learning.social.entity.SysSocialDetails;
import org.apache.ibatis.annotations.Mapper;

/***
 * 社交登陆Mapper
 * @author 刘靖
 * @date 2019/9/23 11:34
 */
@Mapper
public interface SysSocialDetailMapper extends BaseMapper<SysSocialDetails> {
}
