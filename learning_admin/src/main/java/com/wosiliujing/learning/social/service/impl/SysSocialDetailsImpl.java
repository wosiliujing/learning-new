package com.wosiliujing.learning.social.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wosiliujing.learning.social.entity.SysSocialDetails;
import com.wosiliujing.learning.entity.UserInfoDTO;
import com.wosiliujing.learning.handler.LoginHandler;
import com.wosiliujing.learning.social.mapppr.SysSocialDetailMapper;
import com.wosiliujing.learning.social.service.SysSocialDetailsService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 社交登陆service Impl
 * @author 刘靖
 * @date 2019-09-23 14:33
 */
@Slf4j
@Service
@AllArgsConstructor
public class SysSocialDetailsImpl extends ServiceImpl<SysSocialDetailMapper, SysSocialDetails> implements SysSocialDetailsService {

    private final Map<String, LoginHandler> loginHandlerMap;


    @Override
    public Boolean bindSocial(String type, String code) {
        return false;
    }

    @Override
    public UserInfoDTO getUserInfo(String type, String code) {
        return loginHandlerMap.get(type).handle(code);
    }
}
