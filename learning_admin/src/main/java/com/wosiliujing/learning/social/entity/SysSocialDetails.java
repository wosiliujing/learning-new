package com.wosiliujing.learning.social.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/***
 * 社交登陆表
 * @author 刘靖
 * @date 2019/9/23 11:05
 */
@Data
public class SysSocialDetails implements Serializable {

    /**
     * id
     */
    @TableId(type = IdType.UUID)
    private String id;

    /**
     * 类型
     */
    private String type;

    /**
     * 描述
     */
    private String remark;

    /**
     * appId
     */
    private String appId;

    /**
     * appSecret
     */
    private String appSecret;

    /**
     * 重定向地址
     */
    private String redirectUrl;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 逻辑删除字段
     */
    @TableLogic
    private String delFlag;
}
