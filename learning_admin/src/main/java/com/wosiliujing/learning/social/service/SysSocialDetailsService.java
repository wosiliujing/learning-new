package com.wosiliujing.learning.social.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wosiliujing.learning.entity.UserInfoDTO;
import com.wosiliujing.learning.social.entity.SysSocialDetails;

/**
 * 社交登陆service
 * @author 刘靖
 * @date 2019-09-23 11:43
 */
public interface SysSocialDetailsService extends IService<SysSocialDetails> {

    /***
     * 绑定社交登陆
     * @author 刘靖
     * @date 2019/9/23 14:36
     * @param type 社交类型
     * @param code 身份标识码
     * @return  绑定结果
     */
    Boolean bindSocial(String type, String code);

    /***
     * 获取用户信息
     * @author 刘靖
     * @date 2019/9/23 14:40
     * @param type 社交类型
     * @param code 身份标识
     * @return 用户信息
     */
    UserInfoDTO getUserInfo(String type , String code);
}
