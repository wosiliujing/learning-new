package com.wosiliujing.learning.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wosiliujing.learning.user.entity.SysUserRole;

import java.util.List;

/**
 * @author: liujing
 * @date: 2019/5/6 22:21
 * @description:
 */
public interface SysUserRoleService extends IService<SysUserRole> {

    /**
     * 批量保存用户角色关系
     * @param roles 角色ids
     * @param userId 用户id
     * @return 保存结果
     */
    Boolean saveBatch(List<String> roles, String userId);
}
