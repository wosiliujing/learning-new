package com.wosiliujing.learning.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.wosiliujing.learning.annotations.Sensitive;
import com.wosiliujing.learning.constant.enums.SensitiveTypeEnum;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author liujing
 */
@Data
public class SysUser implements Serializable {

    /**
     * id
     */
    @TableId(type = IdType.UUID)
    private String userId;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 密码
     */
    private String password;


    /**
     * 手机
     */
    @Sensitive(type = SensitiveTypeEnum.MOBILE_PHONE)
    private String phone;

    /**
     * 部门id
     */
    private String deptId;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 删除标识
     */
    @TableLogic
    private String delFlag;

    /**
     * 锁定标识
     */
    private String lockFlag;

    /**
     * 头像
     */
    private String avatar;
}
