package com.wosiliujing.learning.user.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wosiliujing.learning.user.entity.SysUserRole;
import com.wosiliujing.learning.user.mapper.SysUserRoleMapper;
import com.wosiliujing.learning.user.service.SysUserRoleService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author: liujing
 * @date: 2019/5/6 22:23
 * @description:
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements SysUserRoleService {
    @Override
    public Boolean saveBatch(List<String> roleIds, String userId) {
        List<SysUserRole> list = roleIds.stream()
                .map(roleId -> {
                    SysUserRole sysUserRole = new SysUserRole();
                    sysUserRole.setRoleId(roleId);
                    sysUserRole.setUserId(userId);
                    return sysUserRole;
                }).collect(Collectors.toList());
        return this.saveBatch(list);
    }
}
