package com.wosiliujing.learning.user.controller;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wosiliujing.learning.annotation.Inner;
import com.wosiliujing.learning.annotation.SysLog;
import com.wosiliujing.learning.constant.CommonConstants;
import com.wosiliujing.learning.entity.SecurityUser;
import com.wosiliujing.learning.user.entity.SysUser;
import com.wosiliujing.learning.entity.UserInfoDTO;
import com.wosiliujing.learning.pojo.dto.UserDTO;
import com.wosiliujing.learning.pojo.dto.UserInfo;
import com.wosiliujing.learning.pojo.dto.UserRestPasswordDTO;
import com.wosiliujing.learning.user.service.SysUserService;
import com.wosiliujing.learning.util.RestResult;
import com.wosiliujing.learning.util.SecurityUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiOperationSupport;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

/**
 * @author liujing
 */
@Api(tags = "User 接口")
@RequestMapping("/user")
@RestController
@AllArgsConstructor
public class SysUserController {

    private final SysUserService sysUserService;

    /**
     * 获取当前用户
     * @return 当前用户信息
     */
    @ApiOperationSupport(author = "张三")
    @GetMapping("/info")
    public RestResult info(){
        SecurityUser user = SecurityUtil.getUser();
        assert user != null;
        SysUser sysUser = sysUserService.getOne(
                Wrappers.<SysUser>lambdaQuery().eq(SysUser::getUserName,user.getUsername()));
        if(ObjectUtil.isEmpty(sysUser)){
            return new RestResult<>();
        }
        UserInfo userInfo = sysUserService.getUserInfo(sysUser);
        return RestResult.data(userInfo);
    }



    /**
     * 通过用户名获取用户
     * @param userName 用户名
     * @return 用户信息
     */
    @Inner
    @RequestMapping("/info/{userName}")
    public RestResult<UserInfoDTO> info(@PathVariable String userName){
        SysUser sysUser = sysUserService.getOne(
                Wrappers.<SysUser>lambdaQuery().eq(SysUser::getUserName,userName));
        if(ObjectUtil.isEmpty(sysUser)){
            return new RestResult<>();
        }
        UserInfo userInfo = sysUserService.getUserInfo(sysUser);
        UserInfoDTO userInfoDto = new UserInfoDTO();
        userInfoDto.setUserName(userInfo.getSysUser().getUserName());
        userInfoDto.setPassword(userInfo.getSysUser().getPassword());
        userInfoDto.setPermission(userInfo.getPermission());
        userInfoDto.setEnhanceMap(new HashMap<>(5));
        return RestResult.data(userInfoDto);
    }

    /**
     * 通过用户名获取用户简单信息
     * @param userName 用户名
     * @return 用户信息
     */
    @Inner
    @GetMapping("/baseInfo/{userName}")
    public RestResult baseInfo(@PathVariable("userName") String userName){
        return  RestResult.data(sysUserService.getOne(
                Wrappers.<SysUser>lambdaQuery().eq(SysUser::getUserName,userName)));
    }

    /**
     * 获取所有用户信息
     * @return 用户信息列表
     */
    @GetMapping("/listAll")
    public RestResult listAll(){
        return RestResult.data(sysUserService.list(Wrappers.emptyWrapper()));
    }
    /**
     * 通过id获取用户展示信息
     * @param id 用户id
     * @return 用户信息
     */
    @ApiOperation(value = "根据id查询user信息")
    @GetMapping("/vo/{id}")
    public RestResult getUserVoById(@PathVariable @ApiParam(name = "id", value = "用户id", required = true) String id){
     return RestResult.data(sysUserService.getUserVoById(id));
    }



    /**
     * 通过id获取用户详情
     * @param id 用户id
     * @return 用户详情
     */
    @GetMapping("/detail/{id}")
    public RestResult getUserDetailById(@PathVariable String id){
        return RestResult.data(sysUserService.getById(id));
    }

    /**
     * 新增用户
     * @param userDto 用户新增信息
     * @return 保存结果
     */
    @SysLog("新增用户")
    @PreAuthorize("@pms.hasPermisssion('sys_user_add')")
    @PostMapping("add")
    public RestResult addUser(@RequestBody UserDTO userDto){
        return  sysUserService.saveUser(userDto)? RestResult.ok(): RestResult.error(CommonConstants.SAVE_FAIL);
    }

    /**
     * 删除用户
     * @param id 用户id
     * @return 删除结果
     */
    @SysLog("删除用户")
    @PreAuthorize("@pms.hasPermisssion('sys_user_del')")
    @DeleteMapping("delete/{id}")
    public RestResult deleteUser(@PathVariable String id){
        return sysUserService.deleteUser(id)? RestResult.ok(): RestResult.error(CommonConstants.DELETE_FAIL);
    }


    /**
     * 分页查询用户展示信息
     * @param page 分页参数
     * @param userDto 查询条件
     * @return 分页数据
     */
    @GetMapping("/page")
    public RestResult getUserVoPage(Page page, UserDTO userDto){
        return RestResult.data(sysUserService.getUserVoPage(page, userDto));
    }

    /**
     * 更新用户信息
     * @param userDto 更新参数
     * @return 更新结果
     */
    @SysLog("更新用户")
    @PreAuthorize("@pms.hasPermisssion('sys_user_edit')")
    @PutMapping("/update")
    public RestResult updateUser(@RequestBody UserDTO userDto){
        return sysUserService.updateUser(userDto)? RestResult.ok(): RestResult.error(CommonConstants.UPDATE_FAIL);
    }

    /**
     * 重置用户密码
     * @param userRestPasswordDto 新旧密码
     * @return 重置结果
     */
    @SysLog("重置用户密码")
    @PreAuthorize("@pms.hasPermisssion('sys_user_rest_password')")
    @PutMapping("/restPassword")
    public RestResult restPassword(@RequestBody UserRestPasswordDTO userRestPasswordDto){
        return sysUserService.restPassword(userRestPasswordDto);
    }

    /**
     * 重置当前用户密码
     * @param userRestPasswordDto 新旧密码
     * @return 重置结果
     */
    @SysLog("重置当前用户密码")
    @PutMapping("/restOwnPassword")
    public RestResult restOwnPassword(@RequestBody UserRestPasswordDTO userRestPasswordDto){
        return sysUserService.restOwnPassword(userRestPasswordDto);
    }

    /**
     * 更新本人用户信息
     * @param userDto 用户信息
     * @return 更新结果
     */
    @SysLog("更新本人用户信息")
    @PutMapping("/updateOwnInfo")
    public RestResult updateOwnInfo(@RequestBody UserDTO userDto){
        return sysUserService.updateOwnInfo(userDto)? RestResult.ok(): RestResult.error(CommonConstants.UPDATE_FAIL);
    }



}
