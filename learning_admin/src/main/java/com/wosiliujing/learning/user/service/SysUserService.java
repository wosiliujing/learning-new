package com.wosiliujing.learning.user.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wosiliujing.learning.pojo.dto.UserDTO;
import com.wosiliujing.learning.pojo.dto.UserInfo;
import com.wosiliujing.learning.pojo.dto.UserRestPasswordDTO;
import com.wosiliujing.learning.user.entity.SysUser;
import com.wosiliujing.learning.pojo.vo.UserVO;
import com.wosiliujing.learning.util.RestResult;

/**
 * @author liujing
 */
public interface SysUserService extends IService<SysUser> {

    /**
     * 获取用户信息
     * @param sysUser 用户
     * @return 用户信息
     */
    UserInfo getUserInfo(SysUser sysUser);

    /**
     * 通过id获取用户展示信息
     * @param userId 用户id
     * @return 用户信息
     */
    UserVO getUserVoById(String userId);

    /**
     * 分页查询用户展示信息
     * @param page 分页参数
     * @param userDto 查询条件
     * @return 分页数据
     */
    IPage getUserVoPage(Page page, UserDTO userDto);

    /**
     * 保存用户信息
     * @param userDto
     * @return 保存结果
     */
    Boolean saveUser(UserDTO userDto);

    /**
     * 删除用户
     * @param userId 用户id
     * @return  删除结果
     */
    Boolean deleteUser(String userId);

    /**
     * 更新用户
     * @param userDto 更新字段
     * @return 更新结果
     */
    Boolean updateUser(UserDTO userDto);

    /**
     * 重置用户密码
     * @param userRestPasswordDto 新旧密码
     * @return 重置结果
     */
    RestResult restPassword(UserRestPasswordDTO userRestPasswordDto);


    /**
     * 重置当前用户密码
     * @param userRestPasswordDto 新旧密码
     * @return 重置结果
     */
    RestResult restOwnPassword(UserRestPasswordDTO userRestPasswordDto);

    /**
     * 更新个人信息
     * @param userDto 用户信息
     * @return 更新结果
     */
    Boolean updateOwnInfo(UserDTO userDto);
}
