package com.wosiliujing.learning.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wosiliujing.learning.user.entity.SysUserRole;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author: liujing
 * @date: 2019/5/5 20:58
 * @description:
 */
@Mapper
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
