package com.wosiliujing.learning.user.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wosiliujing.learning.user.entity.SysUser;
import com.wosiliujing.learning.pojo.vo.UserVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


/**
 * @author liujing
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {

    /**
     * 获取用户信息
     * @param wrapper 查询条件
     * @return 用户展示信息
     */
    UserVO getUserVo(@Param(Constants.WRAPPER) Wrapper wrapper);

    /**
     * 通过条件分页查询用户展示信息
     * @param page 分页参数
     * @param wrapper 查询条件
     * @return
     */
    IPage<UserVO> getUserVoPage(Page page, @Param(Constants.WRAPPER) Wrapper wrapper);
 }
