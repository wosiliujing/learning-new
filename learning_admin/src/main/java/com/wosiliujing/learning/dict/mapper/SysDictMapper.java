package com.wosiliujing.learning.dict.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wosiliujing.learning.dict.entity.SysDict;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author: liujing
 * @date: 2019/5/19 14:09
 * @description:
 */
@Mapper
public interface SysDictMapper extends BaseMapper<SysDict> {
}
