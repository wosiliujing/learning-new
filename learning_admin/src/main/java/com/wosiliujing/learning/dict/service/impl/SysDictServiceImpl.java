package com.wosiliujing.learning.dict.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wosiliujing.learning.dict.entity.SysDict;
import com.wosiliujing.learning.dict.mapper.SysDictMapper;
import com.wosiliujing.learning.dict.service.SysDictService;
import org.springframework.stereotype.Service;

/**
 * @author: liujing
 * @date: 2019/5/19 14:11
 * @description:
 */
@Service
public class SysDictServiceImpl extends ServiceImpl<SysDictMapper,SysDict> implements SysDictService {

}
