package com.wosiliujing.learning.dict.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wosiliujing.learning.dict.entity.SysDict;

/**
 * @author: liujing
 * @date: 2019/5/19 14:10
 * @description:
 */
public interface SysDictService extends IService<SysDict> {
}
