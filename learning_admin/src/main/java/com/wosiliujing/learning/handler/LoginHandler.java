package com.wosiliujing.learning.handler;

import com.wosiliujing.learning.entity.UserInfoDTO;
import com.wosiliujing.learning.pojo.dto.UserInfo;

/**
 * 登陆处理器
 * @author 刘靖
 * @date 2019-09-23 14:51
 */
public interface LoginHandler {

    /***
     * 数据合法校验
     * @author 刘靖
     * @date 2019/9/23 15:03
     * @param code 身份标识
     * @return  校验结果
     */
    Boolean check(String code);

    /***
     * 获取身份唯一标识（userId）
     * @author 刘靖
     * @date 2019/9/23 15:05
     * @param code 身份标识码
     * @return 身份唯一标识（userId）
     */
    String identify(String code);

    /***
     * 获取用户信息
     * @author 刘靖
     * @date 2019/9/23 15:10
     * @param identify 身份唯一标识
     * @return 用户信息
     */
    UserInfoDTO info(String identify);

    /***
     * 登陆处理方法
     * @author 刘靖
     * @date 2019/9/24 8:36
     * @param code 身份唯一标识
     * @return 用户信息
     */
    UserInfoDTO handle(String code);
}
