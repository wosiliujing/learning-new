package com.wosiliujing.learning.handler;

import com.wosiliujing.learning.entity.UserInfoDTO;

/**
 * 社交登陆接口抽象类
 * @author 刘靖
 * @date 2019-09-24 8:37
 */
public  abstract class AbstractLoginHandler implements LoginHandler{

    @Override
    public Boolean check(String code) {
        return true;
    }

    @Override
    public UserInfoDTO handle(String code) {
        if(!check(code)){
            return null;
        }
        return info(identify(code));
    }
}
