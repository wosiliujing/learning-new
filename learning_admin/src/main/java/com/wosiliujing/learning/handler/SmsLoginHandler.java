package com.wosiliujing.learning.handler;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.wosiliujing.learning.user.entity.SysUser;
import com.wosiliujing.learning.entity.UserInfoDTO;
import com.wosiliujing.learning.pojo.dto.UserInfo;
import com.wosiliujing.learning.user.service.SysUserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.HashMap;

/**
 * 手机号，验证码登陆
 * @author 刘靖
 * @date 2019-09-24 8:53
 */
@Slf4j
@Component("SMS")
@AllArgsConstructor
public class SmsLoginHandler extends AbstractLoginHandler {

    private final SysUserService sysUserService;

    @Override
    public String identify(String mobile) {
        return mobile;
    }

    @Override
    public UserInfoDTO info(String identify) {
        SysUser sysUser = sysUserService.getOne(Wrappers.<SysUser>lambdaQuery().eq(SysUser::getPhone,identify));
        if (ObjectUtil.isEmpty(sysUser)){
            log.error("手机号未注册:{}",identify);
            return  null;
        }
        UserInfo userInfo =  sysUserService.getUserInfo(sysUser);
        UserInfoDTO userInfoDto = new UserInfoDTO();
        userInfoDto.setPassword(userInfo.getSysUser().getPassword());
        userInfoDto.setEnhanceMap(new HashMap<>(5));
        userInfoDto.setPermission(userInfo.getPermission());
        userInfoDto.setUserName(userInfo.getSysUser().getUserName());
        return userInfoDto;
    }
}
