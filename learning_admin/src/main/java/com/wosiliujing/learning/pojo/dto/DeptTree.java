package com.wosiliujing.learning.pojo.dto;

import com.wosiliujing.learning.entity.TreeNode;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author: liujing
 * @date: 2019/5/7 20:38
 * @description:
 */
@Data
public class DeptTree extends TreeNode {
    /**
     * 部门名称
     */
    private String name;

    /**
     * 排序
     */
    private String sort;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private  LocalDateTime updateTime;

    /**
     * 父部门id
     */
    private String parentId;
}
