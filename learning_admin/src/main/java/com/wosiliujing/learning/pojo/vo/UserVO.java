package com.wosiliujing.learning.pojo.vo;

import com.wosiliujing.learning.role.entity.SysRole;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/***
 * @author 刘靖
 * @date 2020/6/4 17:19
 */
@Data
public class UserVO {
    /**
     * id
     */
    private String userId;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 手机
     */
    private String phone;

    /**
     * 部门id
     */
    private String deptId;

    /**
     * 部门名称
     */
    private String deptName;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 锁定标识
     */
    private String lockFlag;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 角色
     */
    private List<SysRole> roles;

}
