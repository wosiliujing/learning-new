package com.wosiliujing.learning.pojo.dto;

import lombok.Data;

import java.util.List;

/**
 * @author: liujing
 * @date: 2019/5/7 09:33
 * @description:
 */
@Data
public class UserDTO {

    /**
     * 用户id
     */
    private String userId;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 手机
     */
    private String phone;

    /**
     * 部门id
     */
    private String deptId;

    /**
     * 部门名称
     */
    private String deptName;

    /**
     * 锁定标识
     */
    private String lockFlag;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 角色列表
     */
    private List<String> roles;
}
