package com.wosiliujing.learning.pojo.dto;

import lombok.Data;

/**
 * @author: liujing
 * @date: 2019/5/7 15:25
 * @description:
 */
@Data
public class UserRestPasswordDTO {

    /**
     * 用户id
     */
    private String userId;

    /**
     * 旧密码
     */
    private String oldPassword;

    /**
     * 新密码
     */
    private String newPassword;
}
