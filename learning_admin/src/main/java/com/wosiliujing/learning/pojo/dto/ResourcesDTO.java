package com.wosiliujing.learning.pojo.dto;

import lombok.Data;

/**
 * @author: liujing
 * @date: 2019/5/9 14:19
 * @description:
 */
@Data
public class ResourcesDTO {

    /**
     * id
     */
    private String id;

    /**
     * 资源名称
     */
    private  String name;

    /**
     * 父资源id
     */
    private String parentId;

    /**
     * 权限
     */
    private String permission;

    /**
     * 图标
     */
    private String icon;

    /**
     * 前端路由
     */
    private String path;

    /**
     * 页面组件
     */
    private String component;

    /**
     * 类型（0 菜单,1 按钮，2 功能）
     */
    private String type;

    /**
     * 路由缓冲
     */
    private String keepAlive;

    /**
     * 排序
     */
    private Integer sort;
}
