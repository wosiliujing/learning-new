package com.wosiliujing.learning.pojo.dto;

import com.wosiliujing.learning.entity.TreeNode;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author: liujing
 * @date: 2019/5/9 09:19
 * @description:
 */
@Data
public class ResourcesTree extends TreeNode {

    /**
     * 资源名称
     */
    private  String name;


    /**
     * 权限
     */
    private String permission;

    /**
     * 图标
     */
    private String icon;

    /**
     * 前端路由
     */
    private String path;

    /**
     * 页面组件
     */
    private String component;

    /**
     * 类型（0 菜单,1 按钮，2 功能）
     */
    private String type;

    /**
     * 路由缓存
     */
    private String keepAlive;

    /**
     * 排序
     */
    private Integer sort;


    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private  LocalDateTime updateTime;

}
