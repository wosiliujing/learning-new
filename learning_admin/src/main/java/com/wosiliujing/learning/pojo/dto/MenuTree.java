package com.wosiliujing.learning.pojo.dto;

import com.wosiliujing.learning.entity.TreeNode;
import lombok.Data;

/**
 * @author: liujing
 * @date: 2019/5/8 19:49
 * @description:
 */
@Data
public class MenuTree extends TreeNode {


    /**
     * 资源名称
     */
    private  String name;

    /**
     * 图标
     */
    private String icon;

    /**
     * 前端路由
     */
    private String path;

    /**
     * 页面组件
     */
    private String component;


    /**
     * 路由缓冲
     */
    private String keepAlive;

}
