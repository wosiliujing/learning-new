package com.wosiliujing.learning.pojo.dto;

import com.wosiliujing.learning.user.entity.SysUser;
import lombok.Data;

import java.io.Serializable;
import java.util.Set;

/**
 * @author: liujing
 * @date: 2019/5/6 11:04
 * @description:
 */
@Data
public class UserInfo implements Serializable {

    /**
     * 用户基本信息
     */
    private SysUser sysUser;

    /**
     * 权限
     */
    private Set<String> permission;
}
