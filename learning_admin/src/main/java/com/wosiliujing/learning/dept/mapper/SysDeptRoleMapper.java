package com.wosiliujing.learning.dept.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wosiliujing.learning.dept.entity.SysDeptRole;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author: liujing
 * @date: 2019/5/5 20:55
 * @description:
 */
@Mapper
public interface SysDeptRoleMapper extends BaseMapper<SysDeptRole> {
}
