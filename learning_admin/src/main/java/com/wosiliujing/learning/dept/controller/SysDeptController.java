package com.wosiliujing.learning.dept.controller;

import com.wosiliujing.learning.annotation.SysLog;
import com.wosiliujing.learning.constant.CommonConstants;
import com.wosiliujing.learning.dept.entity.SysDept;
import com.wosiliujing.learning.dept.service.SysDeptService;
import com.wosiliujing.learning.util.RestResult;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

/****
 * @author 刘靖
 * @date 2019/5/7 20:40
 */
@Validated
@RequestMapping("/dept")
@RestController
@AllArgsConstructor
public class SysDeptController {

    private final SysDeptService sysDeptService;

    /**
     * 通过id查询部门信息
     * @param id 部门id
     * @return 部门信息
     */
    @GetMapping("/detail")
    public RestResult findById(@NotBlank(message = "id不能为空") @RequestParam("id") String id){
        return RestResult.data(sysDeptService.getById(id));
    }


    /**
     * 查询全部部门树
     * @return 部门树
     */
    @GetMapping("/tree")
    public RestResult getTree(){
        return RestResult.data(sysDeptService.listDeptTree());
    }

    /**
     * 查询当前用户所属部门部门树
     * @return 部门树
     */
    @GetMapping("/user-tree")
    public RestResult getCurrentUserDeptTree(){
        return RestResult.data(sysDeptService.listCurrentUserDeptTree());
    }


    /**
     * 新增部门
     * @param sysDept 部门信息
     * @return 新增结果
     */
    @SysLog("新增部门")
    @PreAuthorize("@pms.hasPermisssion('sys_dept_add')")
    @PostMapping("/add")
    public RestResult addSysDept(@RequestBody SysDept sysDept){
        return sysDeptService.saveSysDept(sysDept)? RestResult.ok(): RestResult.error(CommonConstants.SAVE_FAIL);
    }

    /**
     * 删除部门
     * @param id 部门id
     * @return 删除结果
     */
    @SysLog("删除部门")
    @PreAuthorize("@pms.hasPermisssion('sys_dept_del')")
    @DeleteMapping("/delete/{id}")
    public RestResult deleteSysDept(@PathVariable String id){
        return sysDeptService.deleteSysDept(id)? RestResult.ok(): RestResult.error(CommonConstants.DELETE_FAIL);
    }

    /**
     * 更新部门
     * @param sysDept 部门信息
     * @return 更新结果
     */
    @SysLog("更新部门")
    @PreAuthorize("@pms.hasPermisssion('sys_dept_edit')")
    @PutMapping("/update")
    public RestResult updateSysDept(@RequestBody SysDept sysDept){
        sysDept.setUpdateTime(LocalDateTime.now());
        return sysDeptService.updateById(sysDept)? RestResult.ok(): RestResult.error(CommonConstants.UPDATE_FAIL);
    }
}
