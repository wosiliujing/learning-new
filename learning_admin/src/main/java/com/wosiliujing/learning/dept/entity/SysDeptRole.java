package com.wosiliujing.learning.dept.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

/**
 * @author: liujing
 * @date: 2019/5/5 17:58
 * @description:
 */
@Data
public class SysDeptRole implements Serializable {

    @TableId(type = IdType.UUID)
    private String id;
    /**
     * 角色id
     */
    private String roleId;

    /**
     * 部门id
     */
    private String deptId;
}
