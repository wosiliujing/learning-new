package com.wosiliujing.learning.dept.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wosiliujing.learning.dept.entity.SysDept;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author: liujing
 * @date: 2019/5/5 20:18
 * @description:
 */
@Mapper
public interface SysDeptMapper extends BaseMapper<SysDept> {
}
