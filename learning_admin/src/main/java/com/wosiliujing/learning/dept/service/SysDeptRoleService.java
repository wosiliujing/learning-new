package com.wosiliujing.learning.dept.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wosiliujing.learning.dept.entity.SysDeptRole;

/**
 * @author: liujing
 * @date: 2019/5/6 21:51
 * @description:
 */
public interface SysDeptRoleService extends IService<SysDeptRole> {
}
