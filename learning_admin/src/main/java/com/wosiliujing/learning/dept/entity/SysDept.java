package com.wosiliujing.learning.dept.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author: liujing
 * @date: 2019/5/5 17:42
 * @description:
 */
@Data
public class SysDept implements Serializable {

    @TableId(type = IdType.UUID)
    private String id;

    /**
     * 部门名称
     */
    private String name;

    /**
     * 排序
     */
    private String sort;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private  LocalDateTime updateTime;

    /**
     * 父部门id
     */
    private String parentId;

    /**
     * 删除标识
     */
    @TableLogic
    private  String delFlag;
}
