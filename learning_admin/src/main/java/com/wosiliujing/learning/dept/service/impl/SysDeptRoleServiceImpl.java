package com.wosiliujing.learning.dept.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wosiliujing.learning.dept.entity.SysDeptRole;
import com.wosiliujing.learning.dept.mapper.SysDeptRoleMapper;
import com.wosiliujing.learning.dept.service.SysDeptRoleService;
import org.springframework.stereotype.Service;

/**
 * @author: liujing
 * @date: 2019/5/6 21:53
 * @description:
 */
@Service
public class SysDeptRoleServiceImpl extends ServiceImpl<SysDeptRoleMapper, SysDeptRole> implements SysDeptRoleService {
}
