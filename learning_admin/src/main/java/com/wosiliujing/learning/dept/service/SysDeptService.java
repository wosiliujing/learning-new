package com.wosiliujing.learning.dept.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wosiliujing.learning.pojo.dto.DeptTree;
import com.wosiliujing.learning.dept.entity.SysDept;

import java.util.List;

/**
 * @author: liujing
 * @date: 2019/5/6 21:49
 * @description:
 */
public interface SysDeptService extends IService<SysDept> {

    /**
     * 获取所有的部门树
     * @return
     */
    List<DeptTree> listDeptTree();

    /**
     * 获取当前用户部门树
     * @return
     */
    List<DeptTree> listCurrentUserDeptTree();

    /**
     * 新增部门
     * @param sysDept 新增信息
     * @return 执行结果
     */
    Boolean saveSysDept(SysDept sysDept);

    /**
     * 删除部门
     * @param deptId 部门id
     * @return 删除结果
     */
    Boolean deleteSysDept(String deptId);
}
