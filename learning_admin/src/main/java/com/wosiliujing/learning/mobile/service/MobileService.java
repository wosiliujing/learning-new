package com.wosiliujing.learning.mobile.service;

import com.wosiliujing.learning.util.RestResult;

/**
 * @author 刘靖
 * @date 2019-09-26 14:32
 */
public interface MobileService {

    /***
     * 发送短信验证码
     * @author 刘靖
     * @param mobile 手机号
     * @date 2019/9/26 14:33
     * @return 短信验证码
     */
    RestResult<String> sendSmsCode(String mobile);
}
