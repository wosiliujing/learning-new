package com.wosiliujing.learning.mobile.controller;

import com.wosiliujing.learning.annotation.Ignore;
import com.wosiliujing.learning.mobile.service.MobileService;
import com.wosiliujing.learning.util.RestResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotBlank;

/**
 * @author 刘靖
 * @date 2019-09-26 15:56
 */
@Api(value = "手机相关接口", tags = "手机相关接口")
@Slf4j
@Validated
@RestController
@RequestMapping("/mobile")
@AllArgsConstructor
public class MobileController {

    private final MobileService mobileService;

    /***
     * 发送手机号验证码
     * @author 刘靖
     * @date 2019/9/26 16:02
     * @param mobile 手机号
     * @return  验证码
     */
    @ApiOperation(value = "发送手机号验证码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "mobile", value = "手机号", paramType= "query")
            })
    @Ignore
    @GetMapping("/sendSms")
    public RestResult sendSms(@NotBlank(message = "手机号不能为空") @RequestParam("mobile") String mobile){
        return  mobileService.sendSmsCode(mobile);
    }


}
