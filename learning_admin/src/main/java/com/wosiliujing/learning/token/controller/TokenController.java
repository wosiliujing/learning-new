package com.wosiliujing.learning.token.controller;

import com.wosiliujing.learning.annotation.SysLog;
import com.wosiliujing.learning.service.RemoteTokenService;
import com.wosiliujing.learning.util.RestResult;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/***
 *
 * @author 刘靖
 * @date 2019/5/19 18:48
 */
@RestController
@AllArgsConstructor
@RequestMapping("/token")
public class TokenController {

    private final RemoteTokenService remoteTokenService;

    /**
     * 分页数据
     * @param params
     * @return
     */
    @GetMapping("/page")
    public RestResult page(@RequestParam Map<String, Object> params){
        return remoteTokenService.getTokenPage(params);
    }

    /**
     * 删除token
     * @param token
     * @return
     */
    @SysLog("删除token")
    @PreAuthorize("@pms.hasPermisssion('sys_token_del')")
    @DeleteMapping("/{token}")
    public RestResult delete(@PathVariable String token){
        return remoteTokenService.deleteToken(token);
    }

}
