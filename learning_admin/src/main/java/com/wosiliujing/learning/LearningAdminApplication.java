package com.wosiliujing.learning;

import com.wosiliujing.learning.annotation.EnableLearningResourceServer;
import com.wosiliujing.learning.annotation.EnableLearningSwagger;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

/**
 * @author liujing
 */
@EnableLearningResourceServer
@SpringCloudApplication
@EnableFeignClients
@EnableLearningSwagger
@EnableCaching
public class LearningAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(LearningAdminApplication.class, args);
    }

    @Bean
    MeterRegistryCustomizer<MeterRegistry> configurer(
            @Value("${spring.application.name}") String applicationName) {
        return (registry) -> registry.config().commonTags("application", applicationName);
    }

}
