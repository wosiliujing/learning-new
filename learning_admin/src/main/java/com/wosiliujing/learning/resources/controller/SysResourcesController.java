package com.wosiliujing.learning.resources.controller;

import com.wosiliujing.learning.pojo.dto.ResourcesDTO;
import com.wosiliujing.learning.resources.entity.SysResources;
import com.wosiliujing.learning.annotation.SysLog;
import com.wosiliujing.learning.constant.CommonConstants;
import com.wosiliujing.learning.resources.service.SysResourcesService;
import com.wosiliujing.learning.util.RestResult;
import com.wosiliujing.learning.util.SecurityUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

/***
 *
 * @author 刘靖
 * @date 2019/5/8 19:16
 */
@Slf4j
@AllArgsConstructor
@RequestMapping("/resources")
@RestController()
public class SysResourcesController {

    private final SysResourcesService sysResourcesService;


    /**
     * 获取当前用户的菜单
     * @return 当前用户菜单树
     */
    @GetMapping("/menu")
    public RestResult getMenu(){
        return RestResult.data(sysResourcesService.menuTrees(SecurityUtil.getPermission()));
    }


    /**
     * 获取资源树
     * @return 资源树
     */
    @GetMapping("/listResources")
    public RestResult listResources(){
        return RestResult.data(sysResourcesService.resourcesTrees());
    }

    /**
     * 获取角色资源信息
     * @param roleId 角色id
     * @return 资源id集合
     */
    @GetMapping("/listResources/{roleId}")
    public RestResult getRoleResources(@PathVariable String roleId){
        return RestResult.data(sysResourcesService.getCurrentUserResourcesIds(roleId));
    }

    /**
     * 通过id获取资源详情
     * @param id 资源id
     * @return 资源详情
     */
    @GetMapping("/detail/{id}")
    public RestResult getResources(@PathVariable String id){
        return RestResult.data(sysResourcesService.getById(id));
    }

    /**
     * 新增资源信息
     * @param resourcesDto 资源信息
     * @return 新增结果
     */
    @SysLog("新增资源信息")
    @PreAuthorize("@pms.hasPermisssion('sys_resources_add')")
    @PostMapping("/save")
    public RestResult save(@RequestBody ResourcesDTO resourcesDto){
        SysResources sysResources = new SysResources();
        BeanUtils.copyProperties(resourcesDto,sysResources);
        sysResources.setDelFlag(CommonConstants.NORMAL_STATUS);
        sysResources.setCreateTime(LocalDateTime.now());
        return sysResourcesService.save(sysResources)? RestResult.ok(): RestResult.error(CommonConstants.SAVE_FAIL);
    }

    /**
     * 更新资源信息
     * @param resourcesDto 资源信息
     * @return 更新结果
     */
    @SysLog("更新资源信息")
    @PreAuthorize("@pms.hasPermisssion('sys_resources_edit')")
    @PutMapping("/update")
    public RestResult update(@RequestBody ResourcesDTO resourcesDto){
        SysResources sysResources = sysResourcesService.getById(resourcesDto.getId());
        BeanUtils.copyProperties(resourcesDto,sysResources);
        sysResources.setUpdateTime(LocalDateTime.now());
        return sysResourcesService.updateById(sysResources)? RestResult.ok(): RestResult.error(CommonConstants.UPDATE_FAIL);
    }

    /**
     * 删除资源信息
     * @param id 资源id
     * @return 删除结果
     */
    @SysLog("删除资源信息")
    @PreAuthorize("@pms.hasPermisssion('sys_resources_del')")
    @DeleteMapping("/delete/{id}")
    public RestResult delete(@PathVariable String id){
        return sysResourcesService.removeById(id)? RestResult.ok(): RestResult.error(CommonConstants.DELETE_FAIL);
    }

}
