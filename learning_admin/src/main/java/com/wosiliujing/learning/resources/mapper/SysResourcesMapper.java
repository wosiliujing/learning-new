package com.wosiliujing.learning.resources.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wosiliujing.learning.resources.entity.SysResources;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author: liujing
 * @date: 2019/5/5 20:53
 * @description:
 */
@Mapper
public interface SysResourcesMapper extends BaseMapper<SysResources> {
}
