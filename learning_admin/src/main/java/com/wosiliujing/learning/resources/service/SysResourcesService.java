package com.wosiliujing.learning.resources.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wosiliujing.learning.pojo.dto.MenuTree;
import com.wosiliujing.learning.pojo.dto.ResourcesTree;
import com.wosiliujing.learning.resources.entity.SysResources;

import java.util.List;


/**
 * @author: liujing
 * @date: 2019/5/6 22:04
 * @description:
 */
public interface SysResourcesService extends IService<SysResources> {

    /**
     * 通过用户拥有的权限获取菜单
     * @param permissions 用户拥有的权限
     * @return 用户的菜单列表
     */
    List<MenuTree> menuTrees(List<String> permissions);

    /**
     * 查询资源树
     * @return 资源树
     */
    List<ResourcesTree> resourcesTrees();

    /**
     * 获取角色拥有的资源列表
     * @param roleId 角色id
     * @return 资源id集合
     */
    List<String> getCurrentUserResourcesIds(String roleId);
}
