package com.wosiliujing.learning.resources.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/***
 * @author 刘靖
 * @date 2020/6/9 13:40
 */
@Data
public class SysResources  implements Serializable {

    @TableId(type = IdType.UUID)
    private  String id;

    /**
     * 资源名称
     */
    private  String name;

    /**
     * 权限
     */
    private String permission;

    /**
     * 父资源id
     */
    private String parentId;

    /**
     * 图标
     */
    private String icon;

    /**
     * 前端路由
     */
    private String path;

    /**
     * 页面组件
     */
    private String component;

    /**
     * 类型（0 菜单,1 按钮，2 功能）
     */
    private String type;

    /**
     * 路由缓冲
     */
    private String keepAlive;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private  LocalDateTime updateTime;

    /**
     * 删除标识
     */
    @TableLogic
    private String delFlag;
}
