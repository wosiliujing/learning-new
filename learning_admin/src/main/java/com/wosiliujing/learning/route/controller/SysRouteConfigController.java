package com.wosiliujing.learning.route.controller;

import cn.hutool.json.JSONArray;
import com.wosiliujing.learning.annotation.SysLog;
import com.wosiliujing.learning.route.service.SysRouteConfigService;
import com.wosiliujing.learning.util.RestResult;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

/***
 *
 * @author 刘靖
 * @date 2019/7/1 16:04
 */
@RestController
@RequestMapping("/route")
@AllArgsConstructor
public class SysRouteConfigController {

    private final SysRouteConfigService  sysRouteConfigService;

    /***
     * 获取当前路由信息
     * @author 刘靖
     * @date 2019/9/17 9:11
     * @return 当前路由信息
     */
    @GetMapping("/list")
    public RestResult list(){
        return RestResult.data(sysRouteConfigService.listRoute());
    }

    /***
     * 更新路由
     * @author 刘靖
     * @date 2019/9/17 9:21
     * @param routes 新的路由列表
     * @return 更新结果
     */
    @SysLog("更新路由")
    @PostMapping("/update")
    public RestResult update(@RequestBody JSONArray routes){
        sysRouteConfigService.updateRoutes(routes);
        return RestResult.ok();
    }

}
