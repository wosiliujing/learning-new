package com.wosiliujing.learning.route.service;

import cn.hutool.json.JSONArray;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wosiliujing.learning.route.entity.SysRouteConfig;
import reactor.core.publisher.Mono;

import java.util.List;

/***
 * @author 刘靖
 * @date 2019/7/1 15:54
 */
public interface SysRouteConfigService extends IService<SysRouteConfig> {


    /***
     * 获取路由列表
     * @author 刘靖
     * @date 2019/9/16 16:06
     * @return 路由列表
     */
    List<SysRouteConfig> listRoute();

    /***
     * 更新路由信息
     * @author 刘靖
     * @date 2019/11/8 10:46
     * @param routes 新的路由列表
     * @return void
     */
    Mono<Void> updateRoutes(JSONArray routes);
}
