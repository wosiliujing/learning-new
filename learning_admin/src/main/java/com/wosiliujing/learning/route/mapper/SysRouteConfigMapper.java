package com.wosiliujing.learning.route.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wosiliujing.learning.route.entity.SysRouteConfig;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author liujing
 * @Description: TODO
 * @date 2019/7/115:53
 */
@Mapper
public interface SysRouteConfigMapper extends BaseMapper<SysRouteConfig> {
}
