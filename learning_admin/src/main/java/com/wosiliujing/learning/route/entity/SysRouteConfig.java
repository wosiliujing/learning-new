package com.wosiliujing.learning.route.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author liujing
 * @Description: TODO
 * @date 2019/6/3011:28
 */
@Data
public class SysRouteConfig implements Serializable {

    /**
     * id
     */
    @TableId(type = IdType.UUID)
    private String id;

    /**
     * 路由id
     */
    private String routeId;

    /**
     * 路由名称
     */
    private String routeName;

    /**
     * 断言
     */
    private String predicates;

    /**
     * 过滤器
     */
    private String filters;

    /**
     * url
     */
    private String url;

    /**
     * 排序
     */
    @TableField(value = "`order`")
    private  Integer order;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 删除标识
     */
    @TableLogic
    private String delFlag;
}
