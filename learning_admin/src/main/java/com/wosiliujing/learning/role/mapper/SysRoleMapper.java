package com.wosiliujing.learning.role.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wosiliujing.learning.role.entity.SysRole;
import com.wosiliujing.learning.pojo.vo.RoleVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author: liujing
 * @date: 2019/5/5 20:48
 * @description:
 */
@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole> {

    /**
     * 角色list
     * @param userId 用户id
     * @return
     */
    @Select("select r.* from sys_role r,sys_user_role ru where r.id = ru.role_id and ru.user_id IN (#{userId})")
    List<SysRole> listRoleByUserId(String userId);

    /**
     * 通过条件查询用户展示信息
     * @param wrapper 查询条件
     * @return 角色展示信息
     */
    RoleVO getRoleVo(@Param(Constants.WRAPPER) Wrapper wrapper);

    /**
     * 通过条件查询分页用户展示信息
     * @param wrapper 查询条件
     * @return 角色展示信息
     */
    List<RoleVO> listRoleVo(@Param(Constants.WRAPPER) Wrapper wrapper);

    /**
     * 通过条件查询分页用户展示信息
     * @param page 分页参数
     * @param wrapper 查询条件
     * @return 角色展示信息
     */
    IPage<RoleVO> listRoleVoPage(Page page, @Param(Constants.WRAPPER) Wrapper wrapper);
}
