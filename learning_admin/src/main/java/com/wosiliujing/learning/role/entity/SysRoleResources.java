package com.wosiliujing.learning.role.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

/**
 * @author: liujing
 * @date: 2019/5/5 18:16
 * @description:
 */
@Data
public class SysRoleResources  implements Serializable {
    @TableId(type = IdType.UUID)
    private String id;

    /**
     * 角色id
     */
    private String roleId;

    /**
     * 资源id
     */
    private String resourcesId;
}
