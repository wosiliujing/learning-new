package com.wosiliujing.learning.role.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wosiliujing.learning.role.entity.SysRoleResources;
import com.wosiliujing.learning.role.mapper.SysRoleResourcesMapper;
import com.wosiliujing.learning.role.service.SysRoleResourcesService;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author: liujing
 * @date: 2019/5/6 22:18
 * @description:
 */
@Service
public class SysRoleResourcesServiceImpl extends ServiceImpl<SysRoleResourcesMapper, SysRoleResources> implements SysRoleResourcesService {
    @Override
    public Boolean saveRoleResources(String roleId, String resourcesIds) {
        this.remove(Wrappers.<SysRoleResources>lambdaQuery().eq(SysRoleResources::getRoleId,roleId));
        if(StrUtil.isBlank(resourcesIds)){
            return Boolean.TRUE;
        }
        List<SysRoleResources> sysRoleResourcesList = Arrays.stream(resourcesIds.split(","))
                .map(resourcesId -> {
                    SysRoleResources sysRoleResources = new SysRoleResources();
                    sysRoleResources.setRoleId(roleId);
                    sysRoleResources.setResourcesId(resourcesId);
                    return sysRoleResources;
                }).collect(Collectors.toList());
        return this.saveBatch(sysRoleResourcesList);
    }
}
