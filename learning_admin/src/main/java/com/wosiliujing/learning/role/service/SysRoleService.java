package com.wosiliujing.learning.role.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wosiliujing.learning.admin.RoleDTO;
import com.wosiliujing.learning.role.entity.SysRole;
import com.wosiliujing.learning.pojo.vo.RoleVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author: liujing
 * @date: 2019/5/6 22:11
 * @description:
 */
public interface SysRoleService extends IService<SysRole> {

    /**
     * 通过id获取角色信息
     * @param roleId 角色id
     * @return 角色信息
     */
    RoleVO getRoleVoById(String roleId);

    /**
     * 保存角色信息
     * @param roleDto 角色信息
     * @return 保存结果
     */
    Boolean saveSysUser(RoleDTO roleDto);

    /**
     * 更新角色信息
     * @param roleDto 角色信息
     * @return  更新结果
     */
    Boolean updateSysRole(RoleDTO roleDto);

    /**
     * 删除角色
     * @param roleId 角色id
     * @return 删除结果
     */
    Boolean deleteSysRole(String roleId);

    /***
     * 获取分页用户展示信息列表
     * @author 刘靖
     * @date 2019/11/8 9:31
     * @param page 分页参数
     * @param wrapper 查询条件
     * @return
     */
    IPage<RoleVO> listRoleVoPage(Page page, @Param(Constants.WRAPPER) Wrapper wrapper);

    /**
     * 获取分页用户展示信息列表
     * @param wrapper 查询条件
     * @return 展示信息列表
     */
    List<RoleVO> listRoleVo(@Param(Constants.WRAPPER) Wrapper wrapper);
}
