package com.wosiliujing.learning.role.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wosiliujing.learning.admin.RoleDTO;
import com.wosiliujing.learning.role.entity.SysRole;
import com.wosiliujing.learning.role.entity.SysRoleResources;
import com.wosiliujing.learning.user.entity.SysUserRole;
import com.wosiliujing.learning.pojo.vo.RoleVO;
import com.wosiliujing.learning.constant.CommonConstants;
import com.wosiliujing.learning.role.mapper.SysRoleMapper;
import com.wosiliujing.learning.role.service.SysRoleResourcesService;
import com.wosiliujing.learning.role.service.SysRoleService;
import com.wosiliujing.learning.user.service.SysUserRoleService;
import lombok.AllArgsConstructor;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.BeanUtils;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

/***
 * @author 刘靖
 * @date 2019/5/6 22:12
 */
@AllArgsConstructor
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper,SysRole> implements SysRoleService {

    private final SysRoleResourcesService sysRoleResourcesService;

    private final SysUserRoleService sysUserRoleService;

    @Override
    @Cacheable(value = "learning_role#200", key = "#roleId")
    public RoleVO getRoleVoById(String roleId) {
        return baseMapper.getRoleVo(Wrappers.<SysRole>lambdaQuery().eq(SysRole::getId, roleId));
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Boolean saveSysUser(RoleDTO roleDto) {
        SysRole sysRole = new SysRole();
        BeanUtils.copyProperties(roleDto,sysRole);
        sysRole.setCreateTime(LocalDateTime.now());
        sysRole.setDelFlag(CommonConstants.NORMAL_STATUS);
        return this.save(sysRole);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Boolean updateSysRole(RoleDTO roleDto) {
        SysRole sysRole = this.getById(roleDto.getId());
        BeanUtils.copyProperties(roleDto,sysRole);
        sysRole.setUpdateTime(LocalDateTime.now());
        return this.updateById(sysRole);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Boolean deleteSysRole(String roleId) {
        sysRoleResourcesService.remove(Wrappers.<SysRoleResources>lambdaQuery().eq(SysRoleResources::getRoleId,roleId));
        sysUserRoleService.remove(Wrappers.<SysUserRole>lambdaQuery().eq(SysUserRole::getRoleId,roleId));
        this.removeById(roleId);
        return Boolean.TRUE;
    }

    @Override
    public IPage<RoleVO> listRoleVoPage(Page page, @Param(Constants.WRAPPER) Wrapper wrapper) {
       return baseMapper.listRoleVoPage(page,wrapper);
    }

    @Override
    public List<RoleVO> listRoleVo(@Param(Constants.WRAPPER) Wrapper wrapper) {
        return baseMapper.listRoleVo(wrapper);
    }
}
