package com.wosiliujing.learning.role.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wosiliujing.learning.role.entity.SysRoleResources;

/**
 * @author 刘靖
 * @date 2019/5/6 22:16
 */
public interface SysRoleResourcesService extends IService<SysRoleResources> {

    /***
     * 保存资源角色
     * @author 刘靖
     * @date 2019/11/8 9:30
     * @param roleId 角色ids
     * @param resourcesIds 资源ids
     * @return 保存结果
     */
    Boolean saveRoleResources(String roleId, String resourcesIds);

}
