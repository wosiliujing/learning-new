package com.wosiliujing.learning.role.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wosiliujing.learning.role.entity.SysRoleResources;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author: liujing
 * @date: 2019/5/5 20:59
 * @description:
 */
@Mapper
public interface SysRoleResourcesMapper extends BaseMapper<SysRoleResources> {


}
