package com.wosiliujing.learning.role.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wosiliujing.learning.admin.RoleDTO;
import com.wosiliujing.learning.annotation.SysLog;
import com.wosiliujing.learning.constant.CommonConstants;
import com.wosiliujing.learning.group.Insert;
import com.wosiliujing.learning.role.service.SysRoleResourcesService;
import com.wosiliujing.learning.role.service.SysRoleService;
import com.wosiliujing.learning.util.RestResult;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;

/***
 * @author 刘靖
 * @date 2019/5/8 22:01
 */
@RequestMapping("/role")
@RestController
@AllArgsConstructor
@Validated
public class SysRoleController {

    private final SysRoleService sysRoleService;
    private final SysRoleResourcesService sysRoleResourcesService;

    /**
     * 通过id获取角色
     * @param id 角色id
     * @return 角色信息
     */
    @GetMapping("/detail/{id}")
    public RestResult getSysRoleById(@PathVariable String id){
        return  RestResult.data(sysRoleService.getRoleVoById(id));
    }

    /**
     * 保存用户角色
     * @param roleDto 角色信息
     * @return 保存结果
     */
    @SysLog("保存用户角色")
    @PreAuthorize("@pms.hasPermisssion('sys_role_add')")
    @PostMapping("/save")
    public RestResult saveSysRole(@RequestBody @Validated(value = {Insert.class}) RoleDTO roleDto){
        return sysRoleService.saveSysUser(roleDto)? RestResult.ok(): RestResult.error(CommonConstants.SAVE_FAIL);
    }

    /**
     * 更新用户角色
     * @param roleDto 角色信息
     * @return 更新结果
     */
    @SysLog("更新用户角色")
    @PreAuthorize("@pms.hasPermisssion('sys_role_edit')")
    @PutMapping("/update")
    public RestResult updateSysRole(@RequestBody @Validated RoleDTO roleDto){
        return sysRoleService.updateSysRole(roleDto)? RestResult.ok(): RestResult.error(CommonConstants.UPDATE_FAIL);
    }

    /**
     * 删除角色
     * @param id 角色id
     * @return 删除结果
     */
    @SysLog("删除角色")
    @PreAuthorize("@pms.hasPermisssion('sys_role_del')")
    @DeleteMapping("/delete/{id}")
    public RestResult deleteSysRole(@PathVariable("id") @NotBlank String id){
        return sysRoleService.deleteSysRole(id)? RestResult.ok(): RestResult.error(CommonConstants.DELETE_FAIL);
    }

    /**
     * 获取角色展示列表
     * @return 角色信息列表
     */
    @GetMapping("/list")
    public RestResult listRoleVo(){
        return RestResult.data(sysRoleService.listRoleVo(Wrappers.emptyWrapper()));
    }

    /**
     * 获取分页角色展示列表
     * @return 角色信息列表
     */
    @GetMapping("/page")
    public RestResult listRoleVoPage(Page page){
        return RestResult.data(sysRoleService.listRoleVoPage(page,Wrappers.emptyWrapper()));
    }

    /**
     * 保存角色资源
     * @param roleId 角色id
     * @param resourcesIds  资源ids
     * @return 保存结果
     */
    @SysLog("保存角色资源")
    @PutMapping("/resources")
    public RestResult saveResources(String roleId, String resourcesIds){
        return sysRoleResourcesService.saveRoleResources(roleId,resourcesIds)? RestResult.ok(): RestResult.error(CommonConstants.SAVE_FAIL);
    }

}
