package com.wosiliujing.learning.client.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author 刘靖
 * @date 2020-06-12 13:52
 */
@Data
@ApiModel(value = "Security客户端新增DTO")
public class SysOauthClientDetailsAddDTO {

    /**
     * 客户端id
     */
    @NotBlank(message = "客户端id不能为空")
    @ApiModelProperty(value = "客户端id")
    private String clientId;

    /**
     * 客户端密钥
     */
    @NotBlank(message = "客户端密钥不能为空")
    @ApiModelProperty(value = "客户端密钥")
    private String clientSecret;

    /**
     * 资源IDS
     */
    @ApiModelProperty(value = "资源IDS")
    private String resourceIds;

    /**
     * 作用域
     */
    @NotBlank(message = "作用域不能为空")
    @ApiModelProperty(value = "作用域")
    private String scope;

    /**
     * 授权方式
     */
    @NotBlank(message = "授权方式不能为空")
    @ApiModelProperty(value = "授权方式")
    private String authorizedGrantTypes;

    /**
     * web重定向url
     */
    @ApiModelProperty(value = "web重定向url")
    private String webServerRedirectUri;

    /**
     * 权限
     */
    @ApiModelProperty(value = "权限")
    private String authorities;

    /**
     * 请求令牌有效时间
     */
    @ApiModelProperty(value = "请求令牌有效时间")
    private Integer accessTokenValidity;

    /**
     * 刷新令牌有效时间
     */
    @ApiModelProperty(value = "刷新令牌有效时间")
    private Integer refreshTokenValidity;

    /**
     * 扩展信息
     */
    @ApiModelProperty(value = "扩展信息")
    private String additionalInformation;

    /**
     * 是否自动放行
     */
    @NotBlank(message = "是否自动放行不能为空")
    @ApiModelProperty(value = "是否自动放行")
    private String autoapprove;

}
