package com.wosiliujing.learning.client.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wosiliujing.learning.client.entity.SysOauthClientDetails;

/**
 * @author: liujing
 * @date: 2019/5/6 21:43
 * @description:
 */
public interface SysOauthClientDetailService extends IService<SysOauthClientDetails> {



}
