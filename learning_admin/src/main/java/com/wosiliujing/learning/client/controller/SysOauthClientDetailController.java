package com.wosiliujing.learning.client.controller;

import com.wosiliujing.learning.annotation.SysLog;
import com.wosiliujing.learning.client.entity.SysOauthClientDetails;
import com.wosiliujing.learning.client.pojo.SysOauthClientDetailsAddDTO;
import com.wosiliujing.learning.client.pojo.SysOauthClientDetailsUpdateDTO;
import com.wosiliujing.learning.client.pojo.SysOauthClientDetailsVO;
import com.wosiliujing.learning.client.service.SysOauthClientDetailService;
import com.wosiliujing.learning.constant.CommonConstants;
import com.wosiliujing.learning.util.PageRequest;
import com.wosiliujing.learning.util.PageResult;
import com.wosiliujing.learning.util.RestResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;

/***
 * 2019/5/19 18:22
 * @author 刘靖
 * @date 2019/11/8 11:21
 */
@Api(value = "security客户端", tags = "security客户端")
@RestController
@AllArgsConstructor
@RequestMapping("/client")
public class SysOauthClientDetailController {

    private final SysOauthClientDetailService sysOauthClientDetailService;

    /**
     * 分页查询数据
     * @param pageRequest 分页参数
     * @return 分页数据
     */
    @ApiOperation(value = "分页查询数据")
    @GetMapping("/page")
    public RestResult<PageResult<SysOauthClientDetailsVO>> findByPage(@Validated PageRequest pageRequest){
        return RestResult.page(sysOauthClientDetailService.page(pageRequest.toSqlRequest()), new SysOauthClientDetailsVO());
    }

    /**
     * 保存客户端信息
     * @param sysOauthClientDetailDto 客户端信息
     * @return 保存结果
     */
    @SysLog("保存客户端信息")
    @PreAuthorize("@pms.hasPermisssion('sys_client_add')")
    @PostMapping("/save")
    public RestResult save(@Validated @RequestBody SysOauthClientDetailsAddDTO sysOauthClientDetailDto){
        SysOauthClientDetails sysOauthClientDetails = new SysOauthClientDetails();
        BeanUtils.copyProperties(sysOauthClientDetailDto, sysOauthClientDetails);
        return sysOauthClientDetailService.save(sysOauthClientDetails)? RestResult.ok(CommonConstants.SAVE_SUCCESS): RestResult.error(CommonConstants.SAVE_FAIL);
    }

    /**
     * 更新客户端信息
     * @param sysOauthClientDetailDto 客户端信息
     * @return 更新结果
     */
    @SysLog("更新客户端信息")
    @PreAuthorize("@pms.hasPermisssion('sys_client_edit')")
    @PutMapping("/update")
    public RestResult update(@Validated @RequestBody SysOauthClientDetailsUpdateDTO sysOauthClientDetailDto){
        SysOauthClientDetails sysOauthClientDetails = new SysOauthClientDetails();
        BeanUtils.copyProperties(sysOauthClientDetailDto, sysOauthClientDetails);
        return sysOauthClientDetailService.updateById(sysOauthClientDetails)? RestResult.ok(CommonConstants.UPDATE_SUCCESS): RestResult.error(CommonConstants.UPDATE_FAIL);
    }

    /**
     * 删除
     * @param id id
     * @return 删除结果
     */
    @SysLog("删除客户端")
    @PreAuthorize("@pms.hasPermisssion('sys_client_del')")
    @DeleteMapping("/delete")
    public RestResult delete(@NotBlank(message = "id不能为空") @RequestParam("id") String id){
        return  sysOauthClientDetailService.removeById(id)? RestResult.ok(CommonConstants.DELETE_SUCCESS): RestResult.error(CommonConstants.DELETE_FAIL);
    }



}
