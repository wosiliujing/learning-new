package com.wosiliujing.learning.client.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wosiliujing.learning.client.entity.SysOauthClientDetails;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author liujing
 */
@Mapper
public interface SysOauthClientDetailMapper extends BaseMapper<SysOauthClientDetails> {
}
