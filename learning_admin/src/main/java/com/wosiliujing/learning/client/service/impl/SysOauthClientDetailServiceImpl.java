package com.wosiliujing.learning.client.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wosiliujing.learning.client.entity.SysOauthClientDetails;
import com.wosiliujing.learning.client.mapper.SysOauthClientDetailMapper;
import com.wosiliujing.learning.client.service.SysOauthClientDetailService;
import org.springframework.stereotype.Service;

/**
 * @author: liujing
 * @date: 2019/5/6 21:44
 * @description:
 */
@Service
public class SysOauthClientDetailServiceImpl extends ServiceImpl<SysOauthClientDetailMapper,SysOauthClientDetails> implements SysOauthClientDetailService {
}
