package com.wosiliujing.learning.client.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 刘靖
 * @date 2020-06-12 11:09
 */
@Data
@ApiModel(value = "Security客户端VO")
public class SysOauthClientDetailsVO {

    /**
     * 客户端id
     */
    @ApiModelProperty(value = "客户端id")
    private String clientId;

    /**
     * 客户端密钥
     */
    @ApiModelProperty(value = "客户端密钥")
    private String clientSecret;

    /**
     * 资源IDS
     */
    @ApiModelProperty(value = "资源IDS")
    private String resourceIds;

    /**
     * 作用域
     */
    @ApiModelProperty(value = "作用域")
    private String scope;

    /**
     * 授权方式
     */
    @ApiModelProperty(value = "授权方式")
    private String authorizedGrantTypes;

    /**
     * web重定向url
     */
    @ApiModelProperty(value = "web重定向url")
    private String webServerRedirectUri;

    /**
     * 权限
     */
    @ApiModelProperty(value = "权限")
    private String authorities;

    /**
     * 请求令牌有效时间
     */
    @ApiModelProperty(value = "请求令牌有效时间")
    private Integer accessTokenValidity;

    /**
     * 刷新令牌有效时间
     */
    @ApiModelProperty(value = "刷新令牌有效时间")
    private Integer refreshTokenValidity;

    /**
     * 扩展信息
     */
    @ApiModelProperty(value = "扩展信息")
    private String additionalInformation;

    /**
     * 是否自动放行
     */
    @ApiModelProperty(value = "是否自动放行")
    private String autoapprove;

}
