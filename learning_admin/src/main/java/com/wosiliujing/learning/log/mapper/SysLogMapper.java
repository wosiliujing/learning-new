package com.wosiliujing.learning.log.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wosiliujing.learning.log.entity.SysLog;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author: liujing
 * @date: 2019/5/17 11:20
 * @description:
 */
@Mapper
public interface SysLogMapper extends BaseMapper<SysLog> {

}
