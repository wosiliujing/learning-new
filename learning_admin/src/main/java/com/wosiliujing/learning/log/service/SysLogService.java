package com.wosiliujing.learning.log.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wosiliujing.learning.log.entity.SysLog;

/**
 * @author: liujing
 * @date: 2019/5/17 11:22
 * @description:
 */
public interface SysLogService extends IService<SysLog> {
}
