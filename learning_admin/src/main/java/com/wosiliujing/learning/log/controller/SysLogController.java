package com.wosiliujing.learning.log.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wosiliujing.learning.admin.SysLogDTO;
import com.wosiliujing.learning.log.entity.SysLog;
import com.wosiliujing.learning.annotation.Inner;
import com.wosiliujing.learning.constant.CommonConstants;
import com.wosiliujing.learning.log.service.SysLogService;
import com.wosiliujing.learning.util.RestResult;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * @author 刘靖
 * @date 2019/5/17 22:33
 */
@AllArgsConstructor
@RestController
@RequestMapping("/log")
public class SysLogController {

    private final SysLogService sysLogService;

    /**
     * 分页查询系统日志
     * @param page 分页参数
     * @param sysLog 分页条件
     * @return 分页日志
     */
    @RequestMapping("/page")
    public RestResult getSysLogPage(Page<SysLog> page, SysLog sysLog){
        return RestResult.data(sysLogService.page(page,Wrappers.query(sysLog)));
    }

    /**
     * 删除日志
     * @param id 日志id
     * @return 删除结果
     */
    @PreAuthorize("@pms.hasPermisssion('sys_log_del')")
    @DeleteMapping("/delete/{id}")
    public RestResult removeById(@PathVariable String id){
        return sysLogService.removeById(id)? RestResult.ok(): RestResult.error(CommonConstants.DELETE_FAIL);
    }

    /**
     * 保存日志
     * @param sysLogDto 日志信息
     * @return 保存结果
     */
    @Inner
    @PostMapping("/save")
    public RestResult save(@RequestBody SysLogDTO sysLogDto){
        SysLog sysLog = new SysLog();
        BeanUtils.copyProperties(sysLogDto, sysLog);
        return sysLogService.save(sysLog)? RestResult.ok(): RestResult.error(CommonConstants.SAVE_FAIL);
    }



}
