package com.wosiliujing.learning.log.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author: liujing
 * @date: 2019/5/17 10:33
 * @description:
 */
@Data
public class SysLog implements Serializable {

    /**
     * id
     */
    @TableId(type = IdType.UUID)
    private String id;

    /**
     * 日志类型
     */
    private String type;

    /**
     * 标题
     */
    private String title;

    /**
     * 操作人
     */
    private String createUserName;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 操作者ip地址
     */
    private String ip;

    /**
     * 用户代理
     */
    private String userAgent;

    /**
     * 请求地址
     */
    private String  requestUrl;

    /**
     * 请求方法类型
     */
    private String method;

    /**
     * 请求参数
     */
    private String params;

    /**
     * 执行时间
     */
    private Long time;

    /**
     * 异常信息
     */
    private String exception;

    /**
     * 客户端id
     */
    private String clientId;

    @TableLogic
    private String delFlag;
}
