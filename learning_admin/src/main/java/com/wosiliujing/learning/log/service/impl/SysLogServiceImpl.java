package com.wosiliujing.learning.log.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wosiliujing.learning.log.entity.SysLog;
import com.wosiliujing.learning.log.mapper.SysLogMapper;
import com.wosiliujing.learning.log.service.SysLogService;
import org.springframework.stereotype.Service;

/**
 * @author: liujing
 * @date: 2019/5/17 11:25
 * @description:
 */
@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper,SysLog> implements SysLogService {
}
