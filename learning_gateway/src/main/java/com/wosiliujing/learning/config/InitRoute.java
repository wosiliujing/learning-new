package com.wosiliujing.learning.config;

import com.wosiliujing.learning.gateway.event.DynamicRouteInitEvent;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.context.WebServerInitializedEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * @author liujing
 * @Description: 初始化路由
 * @date 2019/7/1 14:56
 */
@Component
@AllArgsConstructor
@Slf4j
public class InitRoute  {

    private final ApplicationEventPublisher applicationEventPublisher;

    @Async
    @Order
    @EventListener({WebServerInitializedEvent.class})
    public void init(){
        applicationEventPublisher.publishEvent(new DynamicRouteInitEvent(this));
    }
}
