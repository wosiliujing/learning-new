package com.wosiliujing.learning.config;

import com.wosiliujing.learning.handler.HystrixHandler;
import com.wosiliujing.learning.swagger.SwaggerResourceHandler;
import com.wosiliujing.learning.swagger.SwaggerSecurityHandler;
import com.wosiliujing.learning.swagger.SwaggerUiHandler;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;

/**
 * @author liujing
 */
@Slf4j
@AllArgsConstructor
@Configuration
public class RouterConfig {

    private final HystrixHandler hystrixHandler;
    private final SwaggerResourceHandler swaggerResourceHandler;
    private final SwaggerSecurityHandler swaggerSecurityHandler;
    private final SwaggerUiHandler swaggerUiHandler;

    @Bean
    public RouterFunction routerFunction(){
        return RouterFunctions.route(RequestPredicates.path("/fallback")
                .and(RequestPredicates.accept(MediaType.APPLICATION_JSON_UTF8)),hystrixHandler)
                .andRoute(RequestPredicates.GET("/swagger-resources")
                .and(RequestPredicates.accept(MediaType.ALL)),swaggerResourceHandler)
                .andRoute(RequestPredicates.GET("/swagger-resources/configuration/ui")
                .and(RequestPredicates.accept(MediaType.ALL)),swaggerUiHandler)
                .andRoute(RequestPredicates.GET("/swagger-resources/configuration/security")
                .and(RequestPredicates.accept(MediaType.ALL)),swaggerSecurityHandler);
    }

}
